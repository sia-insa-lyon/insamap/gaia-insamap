from django.apps import AppConfig


class GaiaPublicConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gaia_public'
    verbose_name = "Gaia - Public"
