from django.urls import path
from django.views.generic import TemplateView

from gaia_public.views import MainPageView

urlpatterns = [
    path('', MainPageView.as_view(), name="home_page"),
    path('legal', TemplateView.as_view(template_name="public/mentions_legales.html"), name="mentions_legales"),
]
