from django.shortcuts import render

# Create your views here.
from django.views import View

from gaia_paysans.models import Ferme
from gaia_utils.models import GaiaHypertext,GaiaSetting


class MainPageView(View):
    template_name = "public/front_page.html"

    def get(self, request, *args, **kwargs):
        fermes = Ferme.objects.filter(affichee_publiquement__exact=True, archive=False)
        fragment = GaiaHypertext.find_content("ACCUEIL_SITE")
        tarif_adhesion = GaiaSetting.get_value("TARIF_ADHESION")

        return render(request, self.template_name, {
            "fermes": fermes,
            "ACCUEIL_SITE": fragment,
            "tarif_adhesion": tarif_adhesion
        })


