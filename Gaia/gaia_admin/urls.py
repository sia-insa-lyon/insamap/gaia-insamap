from django.urls import path

from gaia_admin.views import AdminDashboardView, DeliveryDetailsAdminView, ProductRegistrationDetailsAdminView, DeliveryUsersEmailsView, DeliveriesListAdminView, \
    PaymentsListAdminView, SubscriptionsListAdminView, HypertextFragmentEditorAdminView, \
    HypertextFragmentsListAdminView, ScriptingView, SettingsView, PaymentsCsvView, MailingListView, FarmDetailsAdminView, UsersListAdminView, UserDetailsAdminView, FarmsListAdminView, ProductDetailsAdminView

urlpatterns = [
    path('dashboard', AdminDashboardView.as_view(), name="admin_dashboard"),

    path('deliveries', DeliveriesListAdminView.as_view(), name="deliveries_list_admin"),
    path('delivery/<int:delivery_id>', DeliveryDetailsAdminView.as_view(), name="delivery_details_admin"),
    path('deliveries/<int:delivery_id>/emails', DeliveryUsersEmailsView.as_view(), name="delivery_user_emails"),
    path('product_register/<int:registration_id>', ProductRegistrationDetailsAdminView.as_view(), name="product_registration_details_admin"),

    path('subscriptions', SubscriptionsListAdminView.as_view(), name="subscriptions_list_admin"),
    path('accounting/payments', PaymentsListAdminView.as_view(), name="payments_list_admin"),
    path('accounting/payments/csv', PaymentsCsvView.as_view(), name="payments_csv"),

    path('farms', FarmsListAdminView.as_view(), name="farms_list_admin"),
    path('farm/<int:farm_id>', FarmDetailsAdminView.as_view(), name="farm_details_admin"),
    path('product/<int:product_id>', ProductDetailsAdminView.as_view(), name="product_details_admin"),
    path('users', UsersListAdminView.as_view(), name="users_list_admin"),
    path('user/<int:user_id>', UserDetailsAdminView.as_view(), name="user_details_admin"),

    path('fragments', HypertextFragmentsListAdminView.as_view(), name="fragments_list"),
    path('fragments/edit/<str:fragment_id>', HypertextFragmentEditorAdminView.as_view(), name="fragment_editor"),

    path('scripting', ScriptingView.as_view(), name="scripting_view"),
    path('mailing_list', MailingListView.as_view(), name="mailing_list_view"),
    path('settings', SettingsView.as_view(), name="settings_view"),
]
