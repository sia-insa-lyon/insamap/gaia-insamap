from django import forms
from datetime import datetime

class EditHypertextFragmentForm(forms.Form):
    fragment_name = forms.CharField(required=True, empty_value="", max_length=50,
                                    label="Identifiant unique du fragment",
                                    widget=forms.TextInput(attrs={"class": "form-control"}))
    
    fragment_name.widget.attrs['readonly'] = True

    active = forms.BooleanField(required=False, label="Actif ?", initial=False, widget=forms.CheckboxInput(attrs={"class": "form-check-input"}))

    display_name = forms.CharField(required=False, empty_value="", max_length=250,
                                   label="Nom du fragment",
                                   widget=forms.TextInput(attrs={"class": "form-control"}))

    description = forms.CharField(required=False, empty_value="", max_length=1000,
                                  label="Description du fragment",
                                  widget=forms.TextInput(attrs={"class": "form-control"}))

    content = forms.CharField(required=False, empty_value="", label="Contenu du fragment",
                              widget=forms.Textarea(attrs={"class": "form-control", "id": "content_editor"}))

class ExportMailingListForm(forms.Form):
    members = forms.BooleanField(required=False, label="Membres adhérents", label_suffix="", initial=True,
                                 widget=forms.CheckboxInput(attrs={'class': 'form-check-input'}))
    
    years = forms.MultipleChoiceField(choices=[(str(i), str(i)) for i in range(datetime.now().year-1, datetime.now().year+2)], initial=[str(i) for i in range(datetime.now().year-1, datetime.now().year+2)],required=False, label="Années d'adhésion",
                                      widget=forms.SelectMultiple(attrs={'class': 'form-select', 'multiple': 'true'}))
    
    non_paid = forms.BooleanField(required=False, label="N'inclure que les adhérents n'ayant pas payé l'adhésion ?", label_suffix="", initial=False,
                                 widget=forms.CheckboxInput(attrs={'class': 'form-check-input'}))
    
    passed = forms.BooleanField(required=False, label="Membres inscrits à une une distribution dans les", label_suffix="", initial=True,
                                widget=forms.CheckboxInput(attrs={'class': 'form-check-input'}))
    
    duration = forms.IntegerField(required=False, initial=6, min_value=1, max_value=24,
                                  widget=forms.NumberInput(attrs={'class': 'form-control'}))
    
    future =  forms.BooleanField(required=False, label="Membres inscrits à une distribution future", label_suffix="", initial=True,
                                 widget=forms.CheckboxInput(attrs={'class': 'form-check-input'}))
    
    filetype = forms.ChoiceField(choices=[("csv", ".csv"), ("txt", ".txt")], label="Type de fichier", label_suffix=" :", initial="csv",
                                 widget=forms.Select(attrs={'class': 'form-select'}))
    
    def clean(self):

        cleaned_data = super().clean()
        if not cleaned_data.get("members") and not cleaned_data.get("passed") and not cleaned_data.get("future"):
            raise forms.ValidationError("Vous devez sélectionner au moins une option")
        if cleaned_data.get("passed") and not cleaned_data.get("duration"):
            raise forms.ValidationError("Vous devez spécifier une durée pour les membres inscrits à une distribution dans les derniers mois")
        
        return cleaned_data
    
class EditSettingsForm(forms.Form):
    tarif_adhesion = forms.DecimalField(required=True, label="Tarif de l'adhésion", min_value=0, max_value=1000, decimal_places=2,
                                        widget=forms.NumberInput(attrs={"class": "form-control"}))
    
    delai_archivage_distributions = forms.IntegerField(required=True, label="Délai d'archivage des distributions", min_value=0, max_value=365,
                                                        widget=forms.NumberInput(attrs={"class": "form-control"}))
    
    delai_archivage_contrats = forms.IntegerField(required=True, label="Délai d'archivage des contrats", min_value=0, max_value=365,
                                                        widget=forms.NumberInput(attrs={"class": "form-control"}))
    
    quantite_produit_max_utilisateur = forms.IntegerField(required=True, label="Quantité maximale de produit par utilisateur", min_value=0, max_value=1000,
                                                        widget=forms.NumberInput(attrs={"class": "form-control"}))