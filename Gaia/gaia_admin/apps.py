from django.apps import AppConfig


class GaiaAdminConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gaia_admin'
    verbose_name = "Gaia - Admin"
