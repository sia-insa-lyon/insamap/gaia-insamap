from datetime import datetime, timedelta
from typing import Any
from dateutil.relativedelta import relativedelta
from contextlib import redirect_stdout, redirect_stderr
from io import StringIO

from django.db import models
from django.contrib.auth.decorators import login_required, user_passes_test
from django.db.models.query import QuerySet
from django.http import Http404, HttpResponse
from django.shortcuts import redirect, render
from django.db import transaction
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic.list import ListView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template.loader import get_template
from io import BytesIO

from Gaia import settings
from gaia_admin.forms import EditHypertextFragmentForm, ExportMailingListForm, EditSettingsForm
from gaia_compta.models import OperationUtilisateur
from gaia_distribs.models import Distribution, InscriptionAdherent, InscriptionProduit, Contrat
from gaia_distribs.utils import get_current_git_tag_name
from gaia_paysans.models import Ferme, Produit
from gaia_users.models import Utilisateur, Adhesion
from gaia_utils.models import GaiaHypertext, GaiaSetting

class AdminDashboardView(View):
    template_name = "admin/dashboard.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_gestionnaire, login_url="profile_home", redirect_field_name=None))
    def get(self, request):

        fragment  = GaiaHypertext.find_content("DASHBOARD")
        return render(request, self.template_name, {"DASHBOARD": fragment})

class DeliveryUsersEmailsView(View):

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.is_gestionnaire, login_url="home_page", redirect_field_name=None
    ))
    def get(self, request, delivery_id):
        delivery = Distribution.get_or_404(delivery_id)
        registrations = InscriptionAdherent.objects.filter(
            distribution_id=delivery_id
        ).filter(contrat__date_signature__isnull=False)

        file_name = "mails_" + delivery.date_heure.strftime("%d-%m-%Y")
        result_text = ""

        inscription_produit = request.GET.get("inscription_produit")
        if inscription_produit:
            inscription = InscriptionProduit.get_or_404(inscription_produit)
            registrations = registrations.filter(inscription_produit__id=inscription.id)
            file_name += "_" + inscription.produit.nom.replace(" ", "_")
            
        registrations = registrations.distinct("utilisateur_id")

        
        for registration in registrations:
            if len(result_text) > 0:
                result_text += ", "
            result_text += registration.utilisateur.email
        
        response = HttpResponse(result_text, content_type='text/plain')
        response['Content-Disposition'] = 'attachment; filename="{}"'.format(file_name + ".txt")
        return response

class SubscriptionsListAdminView(ListView):
    template_name = "admin/subscriptions/subscriptions_list.html"
    paginate_by = 25

    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_gestionnaire, login_url="home_page", redirect_field_name=None))
    def get(self, request, post_success=None):
        year = request.GET.get("year")
        user = request.GET.get("user_id")
        adhesions = Adhesion.objects.filter(utilisateur__is_active=True)
        
        if year:
            try:
                year = int(year)
                adhesions = adhesions.filter(annee__exact=year)
            except (ValueError, TypeError):
                raise Http404()
        else:
            adhesions = adhesions.filter(annee__gte=datetime.now().year - 1).filter(annee__lte=datetime.now().year + 1).order_by("-date_enregistrement")
            
        if user:
            try:
                user = int(user)
                Utilisateur.get_or_404(user)
                adhesions = adhesions.filter(utilisateur__id__exact=user)
            except (ValueError, TypeError):
                raise Http404()

        paginator = Paginator(adhesions, self.paginate_by)
        page = request.GET.get('page')

        try:
            adhesions = paginator.page(page)
        except PageNotAnInteger:
            adhesions = paginator.page(1)
        except EmptyPage:
            adhesions = paginator.page(paginator.num_pages)

        context = {
            "adhesions": adhesions,
            "years": [str(i) for i in range(datetime.now().year - 1, datetime.now().year + 2)]
        }
        if post_success == True:
            context["success"] = True
        elif post_success == False:
            context["error"] = True
        return render(request, self.template_name, context)

    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_gestionnaire, login_url="home_page", redirect_field_name=None))
    def post(self, request):
        subscription_id = request.POST.get("subscription_id")
        success = False
        if subscription_id:
            try:
                subscription_id = int(subscription_id)
                subscription = Adhesion.objects.get(id=subscription_id)
                subscription.payee = True
                subscription.save()
                success = True
            except (ValueError, Adhesion.DoesNotExist):
                success = False
        return self.get(request, post_success=success)

class DeliveriesListAdminView(View):
    template_name = "admin/deliveries/deliveries_list.html"
    paginate_by=25

    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_gestionnaire, login_url="profile_home", redirect_field_name=None))
    def get(self, request):
        past_deliveries = Distribution.objects.filter(date_heure__lt=(datetime.now()-models.F('duree'))).order_by("-date_heure")
        future_deliveries = Distribution.objects.filter(date_heure__gte=(datetime.now()-models.F('duree'))).order_by("date_heure")

        past_paginator = Paginator(past_deliveries, self.paginate_by)
        future_paginator = Paginator(future_deliveries, self.paginate_by)

        past_page_number = self.request.GET.get('past_page')
        future_page_number = self.request.GET.get('future_page')

        try:
            past_page = past_paginator.page(past_page_number)
        except PageNotAnInteger:
            past_page = past_paginator.page(1)
        except EmptyPage:
            past_page = past_paginator.page(past_paginator.num_pages)

        try:
            future_page = future_paginator.page(future_page_number)
        except PageNotAnInteger:
            future_page = future_paginator.page(1)
        except EmptyPage:
            future_page = future_paginator.page(future_paginator.num_pages)

        return render(request, self.template_name, {
            "past_page": past_page ,
            "future_page": future_page
        })

class DeliveryDetailsAdminView(ListView):
    template_name = "admin/deliveries/delivery_details.html"
    model = InscriptionProduit
    context_object_name = "inscriptions"
    paginate_by=25

    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_gestionnaire, login_url="profile_home", redirect_field_name=None))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)
    

    def get_queryset(self):
        delivery_id = self.kwargs['delivery_id']
        incriptions = InscriptionProduit.objects.filter(distribution_id__exact=delivery_id)
        return incriptions.order_by("produit__ferme__nom", "produit__nom")
    
    def get_context_data(self, **kwargs: Any):
        context = super().get_context_data(**kwargs)
        delivery_id = self.kwargs['delivery_id']
        context["delivery"] = Distribution.get_or_404(delivery_id)
        return context

class ProductRegistrationDetailsAdminView(ListView):
    template_name = "admin/deliveries/product_registration_details.html"
    model = InscriptionAdherent
    context_object_name = "inscriptions"
    paginate_by=25

    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_gestionnaire, login_url="profile_home", redirect_field_name=None))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)
    

    def get_queryset(self):
        registration_id = self.kwargs['registration_id']
        incriptions = InscriptionAdherent.objects.filter(inscription_produit__id__exact=registration_id).filter(contrat__date_signature__isnull=False)
        return incriptions.order_by("utilisateur__last_name", "utilisateur__first_name")
    
    def get_context_data(self, **kwargs: Any):
        context = super().get_context_data(**kwargs)
        registration_id = self.kwargs['registration_id']
        context["registration"] = InscriptionProduit.get_or_404(registration_id)
        return context

class FarmsListAdminView(View):
    template_name = "admin/farms/farms_list.html"
    paginate_by = 25

    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_gestionnaire, login_url="profile_home", redirect_field_name=None))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request):
        if request.GET.get("search"):
            try:
                search = int(request.GET.get("search"))
                Ferme.get_or_404(search)
                return redirect("farm_details_admin", farm_id=search)
            except (ValueError, TypeError):
                raise Http404()
        
        farms_list = Ferme.objects.order_by("nom")
        if not "archive" in request.GET:
            farms_list = farms_list.filter(archive=False)
    
        paginator = Paginator(farms_list, self.paginate_by)
        page = request.GET.get('page')
        try:
            farms = paginator.page(page)
        except PageNotAnInteger:
            farms = paginator.page(1)
        except EmptyPage:
            farms = paginator.page(paginator.num_pages)

        return render(request, self.template_name, {'farms': farms})

class FarmDetailsAdminView(ListView):
    template_name = "admin/farms/farm_details.html"
    model = Produit
    context_object_name = "products"
    paginate_by=25

    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_gestionnaire, login_url="profile_home", redirect_field_name=None))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_queryset(self):
        farm_id = self.kwargs['farm_id']
        products = Produit.objects.filter(ferme_id__exact=farm_id)
        if Ferme.get_or_404(farm_id).archive == False:
            products = products.filter(archive=False)
        return products.order_by("nom")

    def get_context_data(self, **kwargs: Any):
        context = super().get_context_data(**kwargs)
        farm_id = self.kwargs['farm_id']
        context["farm"] = Ferme.get_or_404(farm_id)
        return context
    
class ProductDetailsAdminView(ListView):
    template_name = "admin/farms/product_details.html"
    model = InscriptionProduit
    context_object_name = "inscriptions"
    paginate_by=25

    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_gestionnaire, login_url="profile_home", redirect_field_name=None))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_queryset(self):
        product_id = self.kwargs['product_id']
        incriptions = InscriptionProduit.objects.filter(produit_id__exact=product_id, distribution__archive=False)
        return incriptions.order_by("-distribution__date_heure")
    
    def get_context_data(self, **kwargs: Any):
        context = super().get_context_data(**kwargs)
        product_id = self.kwargs['product_id']
        context["product"] = Produit.get_or_404(product_id)
        return context

class UsersListAdminView(View):
    template_name = "admin/accounting/users_list.html"
    paginate_by = 25

    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_gestionnaire, login_url="profile_home", redirect_field_name=None))
    def get(self, request):
        if request.GET.get("search"):
            try:
                search = int(request.GET.get("search"))
                Utilisateur.get_or_404(search)
                return redirect("user_details_admin", user_id=search)
            except (ValueError, TypeError):
                raise Http404()

        users_list = Utilisateur.objects.order_by("last_name", "first_name")
        if not "active" in request.GET:
            users_list = users_list.filter(is_active=True)
        
        paginator = Paginator(users_list, self.paginate_by)
        page = request.GET.get('page')
        try:
            users = paginator.page(page)
        except PageNotAnInteger:
            users = paginator.page(1)
        except EmptyPage:
            users = paginator.page(paginator.num_pages)

        return render(request, self.template_name, {'users': users})

class UserDetailsAdminView(ListView):
    template_name = "admin/accounting/user_details.html"
    model = Contrat
    context_object_name = "contrats"
    paginate_by=25

    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_gestionnaire, login_url="profile_home", redirect_field_name=None))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)
    
    def get_queryset(self):
        user_id = self.kwargs['user_id']
        contrats = Contrat.objects.filter(souscripteur_id__exact=user_id, date_signature__isnull=False)
        return contrats.order_by("-date_signature")

    def get_context_data(self, **kwargs: Any):
        context = super().get_context_data(**kwargs)
        user_id = self.kwargs['user_id']
        context["user_profile"] = Utilisateur.get_or_404(user_id)
        return context

class PaymentsListAdminView(View):
    template_name = "admin/accounting/payments_list.html"
    paginate_by=25

    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_gestionnaire, login_url="profile_home", redirect_field_name=None))
    def get(self, request, *args, **kwargs):
        context = self.get_context(request)
        return render(request, self.template_name, context)

    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_gestionnaire, login_url="profile_home", redirect_field_name=None))
    def post(self, request, *args, **kwargs):
        operation_id = request.POST.get("operation_id")
        context = self.get_context(request)
        if operation_id:
            try:
                operation_id = int(operation_id)
                operation = OperationUtilisateur.get_or_404(operation_id)
                operation.statut = request.POST.get("statut")
                operation.mode_paiement = request.POST.get("mode-paiement") if request.POST.get("mode-paiement") else None
                operation.save()
                context["success"] = True
            except (ValueError):
                context["error"] = True
        
        return render(request, self.template_name, context)

    def get_context(self ,request):
        ferme_filter = request.GET.get("ferme")
        user_filter = request.GET.get("user_id")
        contrat = request.GET.get("contrat")

        operations = OperationUtilisateur.objects.order_by("-date_enregistrement")

        if ferme_filter:
            try:
                ferme_filter = self.parse_ferme_filter(ferme_filter)
                if ferme_filter is not None:
                    operations = operations.filter(
                        ferme_id=ferme_filter if ferme_filter != "AMAP" else None
                    )
            except (ValueError, TypeError):
                raise Http404()

        if user_filter and user_filter != "no":
            try:
                user_filter = int(user_filter)
                Utilisateur.get_or_404(user_filter)
                operations = operations.filter(
                    utilisateur__id__exact=user_filter
                )
            except (ValueError, TypeError):
                raise Http404()
            
        if contrat:
            try:
                id_contrat = int(contrat)
                operations = operations.filter(contrat_id__exact=id_contrat)
            except (ValueError, TypeError):
                raise Http404()

        paginator = Paginator(operations, self.paginate_by)
        page = request.GET.get('page')
        try:
            operations = paginator.page(page)
        except PageNotAnInteger:
            operations = paginator.page(1)
        except EmptyPage:
            operations = paginator.page(paginator.num_pages)
        
        context = {
            "operations": operations,
            "fermes": Ferme.objects.all(),
        }

        return context
    
    def parse_ferme_filter(self, ferme_filter):

        if ferme_filter == "AMAP":
            return "AMAP"

        if len(ferme_filter) == 0:
            return None

        try:
            ferme_filter = int(ferme_filter)
            ferme = Ferme.get_or_404(ferme_filter)
            return ferme.id
        except (ValueError, TypeError):
            raise Http404()

class PaymentsCsvView(View):
    template_name = "paysans/accounting/operations_list_csv.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_gestionnaire, login_url="profile_home", redirect_field_name=None))
    def get(self, request):

        ferme_filter = request.GET.get("ferme")
        user_filter = request.GET.get("user_id")
        contrat = request.GET.get("contrat")

        operations = OperationUtilisateur.objects.order_by("-date_enregistrement")

        if ferme_filter:
            try:
                ferme_filter = self.parse_ferme_filter(ferme_filter)
                if ferme_filter is not None:
                    operations = operations.filter(
                        ferme_id=ferme_filter if ferme_filter != "AMAP" else None
                    )
            except (ValueError, TypeError):
                raise Http404()

        if user_filter and user_filter != "no":
            try:
                user_filter = int(user_filter)
                Utilisateur.get_or_404(user_filter)
                operations = operations.filter(
                    utilisateur__id__exact=user_filter
                )
            except (ValueError, TypeError):
                raise Http404()
            
        if contrat:
            try:
                id_contrat = int(contrat)
                operations = operations.filter(contrat_id__exact=id_contrat)
            except (ValueError, TypeError):
                raise Http404()

        payments_infos = {
            "operations": operations
        }

        template = get_template(self.template_name)
        csv = template.render(payments_infos)

        result = BytesIO(csv.encode("Windows-1252"))
        return HttpResponse(result.getvalue(), content_type="text/csv")


class ScriptingView(View):

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        lambda user: user.is_superuser, login_url="home_page", redirect_field_name=None
    ))
    def get(self, request):
        version_tag = get_current_git_tag_name()
        version = settings.VERSION_STRING + ("-" + version_tag if len(version_tag) > 0 else "")

        return render(request, "admin/scripting.html", {
            "version": version, "script_source": "", "script_result": ""
        })

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        lambda user: user.is_superuser, login_url="home_page", redirect_field_name=None
    ))
    def post(self, request):
        version_tag = get_current_git_tag_name()
        version = settings.VERSION_STRING + ("-" + version_tag if len(version_tag) > 0 else "")

        script = request.POST.get("script-source", None)
        if script:
            output = StringIO()
            with redirect_stdout(output):
                with redirect_stderr(output):
                    exec(script)
            script_result = output.getvalue()
            script_source = script
        else:
            script_source = ""
            script_result = ""

        return render(request, "admin/scripting.html", {
            "version": version, "script_source": script_source, "script_result": script_result
        })

class HypertextFragmentsListAdminView(View):
    template_name = "admin/hypertext_fragments/fragments_list.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.is_gestionnaire, login_url="home_page", redirect_field_name=None
    ))
    def get(self, request):
        fragments = GaiaHypertext.objects.order_by("name").all()

        return render(request, self.template_name, {
            "fragments": fragments
        })

class HypertextFragmentEditorAdminView(View):
    template_name = "admin/hypertext_fragments/edit_fragment.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.is_gestionnaire, login_url="home_page", redirect_field_name=None
    ))
    def get(self, request, fragment_id):
        
        form = EditHypertextFragmentForm()
        form.fields["fragment_name"].initial = fragment_id

        fragment = GaiaHypertext.get_or_404(fragment_id)
        form.fields["display_name"].initial = fragment.display_name
        form.fields["active"].initial = fragment.active
        form.fields["description"].initial = fragment.description
        form.fields["content"].initial = fragment.content

        return render(request, self.template_name, {
            "form": form
        })

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.is_gestionnaire, login_url="home_page", redirect_field_name=None
    ))
    def post(self, request, fragment_id):
        form = EditHypertextFragmentForm(request.POST)

        success = False
        if form.is_valid():
            frag_name = form.cleaned_data["display_name"]
            frag_active = form.cleaned_data["active"]
            frag_desc = form.cleaned_data["description"]
            frag_content = form.cleaned_data["content"]

            try:
                fragment = GaiaHypertext.get_or_404(fragment_id)
                fragment.display_name = frag_name
                fragment.active = frag_active
                fragment.description = frag_desc
                fragment.content = frag_content
                fragment.save()
                success = True
            except Exception as e:
                form.add_error(None, "Une erreur inconnue est survenue lors de la sauvegarde du fragment.")

        return render(request, self.template_name, {
            "form": form,
            "success": success
        })

class MailingListView(View):
    template_name = "admin/accounting/mailing_list.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_gestionnaire, login_url="profile_home", redirect_field_name=None))
    def get(self, request):
        return render(request, self.template_name, {
            "form": ExportMailingListForm()
        })
    
    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_gestionnaire, login_url="profile_home", redirect_field_name=None))
    def post(self, request):
        form = ExportMailingListForm(request.POST)
        mails = set()
        if form.is_valid():
            if form.cleaned_data["members"]:
                for year in form.cleaned_data["years"]:
                    if form.cleaned_data["non_paid"]:
                        mails.update(Adhesion.objects.filter(annee__exact=year).filter(payee=False).values_list('utilisateur__email', flat=True))
                    else:
                        mails.update(Adhesion.objects.filter(annee__exact=year).values_list('utilisateur__email', flat=True))
            if form.cleaned_data["passed"]:
                duration = form.cleaned_data["duration"]
                mails.update(InscriptionAdherent.objects.filter(distribution__date_heure__gte=datetime.now()-relativedelta(months=duration)).filter(distribution__date_heure__lte=datetime.now()).filter(contrat__date_signature__isnull=False).values_list('utilisateur__email', flat=True))
            if form.cleaned_data["future"]:
                mails.update(InscriptionAdherent.objects.filter(distribution__date_heure__gte=datetime.now()).filter(contrat__date_signature__isnull=False).values_list('utilisateur__email', flat=True))
            extension = form.cleaned_data["filetype"]
            response = HttpResponse(self.generate_text(mails, extension), content_type='text/plain')
            response['Content-Disposition'] = 'attachment; filename="{}"'.format("mails." + extension)
            return response
        else:
            return render(request, self.template_name, {"form": form})
    
    def generate_text(self, mails, extension):
        text = ""
        i = 0
        for mail in mails:
            text += mail
            i += 1
            if i < len(mails):
                if extension == "csv":
                    text += ", "
                else:
                    text += "\n"
        return text
    
class SettingsView(View):
    template_name = "admin/settings.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        lambda user: user.is_superuser, login_url="home_page", redirect_field_name=None
    ))
    def get(self, request):
        form = EditSettingsForm()

        form.fields["tarif_adhesion"].initial = GaiaSetting.get_value("TARIF_ADHESION")
        form.fields["delai_archivage_distributions"].initial = GaiaSetting.get_value("DELAI_ARCHIVAGE_DISTRIBUTIONS")
        form.fields["delai_archivage_contrats"].initial = GaiaSetting.get_value("DELAI_ARCHIVAGE_CONTRATS")
        form.fields["quantite_produit_max_utilisateur"].initial = GaiaSetting.get_value("QUANTITE_PRODUIT_MAX_UTILISATEUR")

        return render(request, self.template_name, {
            "form": form
        })
    
    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        lambda user: user.is_superuser, login_url="home_page", redirect_field_name=None
    ))
    def post(self, request):
        form = EditSettingsForm(request.POST)

        success = False
        if form.is_valid():
            try:
                with transaction.atomic():
                    tarif_adhesion = GaiaSetting.objects.get(key="TARIF_ADHESION")
                    tarif_adhesion.value = str(round(float(form.cleaned_data["tarif_adhesion"]), 2))
                    tarif_adhesion.save()
                    delai_archivage_distributions = GaiaSetting.objects.get(key="DELAI_ARCHIVAGE_DISTRIBUTIONS")
                    delai_archivage_distributions.value = str(int(form.cleaned_data["delai_archivage_distributions"]))
                    delai_archivage_distributions.save()
                    delai_archivage_contrats = GaiaSetting.objects.get(key="DELAI_ARCHIVAGE_CONTRATS")
                    delai_archivage_contrats.value = str(int(form.cleaned_data["delai_archivage_contrats"]))
                    delai_archivage_contrats.save()
                    quantite_produit_max_utilisateur = GaiaSetting.objects.get(key="QUANTITE_PRODUIT_MAX_UTILISATEUR")
                    quantite_produit_max_utilisateur.value = str(int(form.cleaned_data["quantite_produit_max_utilisateur"]))
                    quantite_produit_max_utilisateur.save()
                    success = True
            except Exception as e:
                form.add_error(None, "Une erreur inconnue est survenue lors de la sauvegarde du paramètre." + str(e))

        return render(request, self.template_name, {
            "form": form,
            "success": success
        })