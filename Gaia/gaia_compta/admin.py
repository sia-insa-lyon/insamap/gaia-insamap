from django.contrib import admin

# Register your models here.
from gaia_compta.models import OperationUtilisateur

class StatutFilter(admin.SimpleListFilter):
    title = "statut"
    parameter_name = "statut"

    def lookups(self, request, model_admin):
        return [
            ("enregistree", "Enregistrée"),
            ("utilisateur_paye", "Marquée payée par l'utilisateur"),
            ("amap_recu", "Reçu par l'AMAP"),
            ("ferme_recu", "Reçu par la ferme"),
        ]

    def queryset(self, request, queryset):
        if self.value() == "enregistree":
            return queryset.filter(statut=OperationUtilisateur.Statuts.OPERATION_ENREGISTREE)
        elif self.value() == "utilisateur_paye":
            return queryset.filter(statut=OperationUtilisateur.Statuts.UTILISATEUR_PAYE)
        elif self.value() == "amap_recu":
            return queryset.filter(statut=OperationUtilisateur.Statuts.AMAP_RECU)
        elif self.value() == "ferme_recu":
            return queryset.filter(statut=OperationUtilisateur.Statuts.FERME_RECU)
        else:
            return queryset

class PaiementFilter(admin.SimpleListFilter):
    title = "moyen de paiement"
    parameter_name = "mode_paiement"

    def lookups(self, request, model_admin):
        return [
            ("carte", "Carte bancaire"),
            ("especes", "Espèces"),
            ("cheque", "Chèque"),
            ("virement", "Virement"),
        ]

    def queryset(self, request, queryset):
        if self.value() == "carte":
            return queryset.filter(mode_paiement=OperationUtilisateur.ModesPaiement.CARTE)
        elif self.value() == "especes":
            return queryset.filter(mode_paiement=OperationUtilisateur.ModesPaiement.ESPECES)
        elif self.value() == "cheque":
            return queryset.filter(mode_paiement=OperationUtilisateur.ModesPaiement.CHEQUE)
        elif self.value() == "virement":
            return queryset.filter(mode_paiement=OperationUtilisateur.ModesPaiement.VIREMENT)
        else:
            return queryset
        
@admin.register(OperationUtilisateur)
class OperationUtilisateurAdminPanel(admin.ModelAdmin):
    date_hierarchy = 'date_enregistrement'
    fieldsets = (
        (None, {
            "fields": ("utilisateur", "ferme", "contrat",)
        })
        ,
        ("Informations comptabilité", {
            "fields": ("montant", "date_enregistrement", "libelle", "statut", "mode_paiement",)
        }),
        ("Avancé", {
            "fields": ("previous_statut",),
            "classes": ("collapse",)
        }))
    list_display = ("utilisateur", "ferme", "contrat", "date_enregistrement",)
    list_display_links = ("utilisateur", "ferme", "contrat",)
    radio_fields = {"statut": admin.HORIZONTAL, "mode_paiement": admin.HORIZONTAL, "previous_statut": admin.HORIZONTAL}
    autocomplete_fields = ("utilisateur", "ferme", "contrat",)
    list_filter = (StatutFilter, PaiementFilter)