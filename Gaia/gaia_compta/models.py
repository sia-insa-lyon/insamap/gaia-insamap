from django.db import models
from django.http import Http404

from Gaia import settings

'''
class Paiement(models.Model):
    date_reception =
    date_encaissement = 
'''


class OperationUtilisateur(models.Model):
    class Statuts(models.IntegerChoices):
        OPERATION_ENREGISTREE = 1
        UTILISATEUR_PAYE = 2
        AMAP_RECU = 3
        FERME_RECU = 4

    class ModesPaiement(models.IntegerChoices):
        CARTE = 1
        ESPECES = 2
        CHEQUE = 3
        VIREMENT = 4

    montant = models.FloatField(verbose_name="Montant de l'opération", null=False, blank=False,
                                help_text="Positif pour un paiement de l'utilisateur vers les fermes, négatif pour l'inverse")
    ferme = models.ForeignKey("gaia_paysans.Ferme", on_delete=models.PROTECT, null=True, blank=True,
                              related_name="operations_utilisateurs")
    contrat = models.ForeignKey("gaia_distribs.Contrat", on_delete=models.CASCADE, null=True, blank=True,
                                related_name="operations_utilisateurs")
    utilisateur = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=False,
                                    related_name="operations")
    date_enregistrement = models.DateTimeField(verbose_name="Date d'enregistrement", null=False, blank=False)
    libelle = models.CharField(verbose_name="Libellé", max_length=128, null=False, blank=True)

    statut = models.IntegerField(choices=Statuts.choices, default=Statuts.OPERATION_ENREGISTREE)
    mode_paiement = models.IntegerField(choices=ModesPaiement.choices, null=True, blank=True)
    previous_statut = models.IntegerField(choices=Statuts.choices, null=True, blank=True,
                                          help_text="Utilisé en interne lorsqu'on annule la réception d'une transaction.")

    def __str__(self):
        return "Opération du {0} pour {1}€ ({2} - {3}) : {4}".format(
            self.date_enregistrement.strftime("%d/%m/%Y"), round(self.montant, 2),
            "INSAMAP" if self.ferme is None else self.ferme.nom,
            self.utilisateur.email if self.utilisateur else "[None]", self.libelle
        )

    def get_debiteur_label(self):
        """
        Returns the display name of the entity paying the operation (the entity that has to pay something to the other party)
        """
        if self.montant > 0:
            if self.utilisateur:
                return "{0} {1}".format(self.utilisateur.last_name, self.utilisateur.first_name)
            else:
                return "Utilisateur supprimé"
        else:
            if self.ferme is None:
                return "INS'AMAP"
            else:
                return self.ferme.nom

    def get_crediteur_label(self):
        """
        Returns the display name of the entity receiving money from the operation
        """
        if self.montant > 0:
            if self.ferme is None:
                return "INS'AMAP"
            else:
                return self.ferme.nom
        else:
            if self.utilisateur:
                return "{0} {1}".format(self.utilisateur.last_name, self.utilisateur.first_name)
            else:
                return "Utilisateur supprimé"

    def get_status_label(self):
        if self.statut == OperationUtilisateur.Statuts.OPERATION_ENREGISTREE:
            return "Opération enregistrée"
        elif self.statut == OperationUtilisateur.Statuts.UTILISATEUR_PAYE:
            return "Marquée payée par l'utilisateur"
        elif self.statut == OperationUtilisateur.Statuts.AMAP_RECU:
            return "Payé à l'AMAP"
        elif self.statut == OperationUtilisateur.Statuts.FERME_RECU:
            return "Payé à la ferme"
        
    def get_paiement_label(self):
        if self.mode_paiement == OperationUtilisateur.ModesPaiement.CARTE:
            return "Carte bancaire"
        elif self.mode_paiement == OperationUtilisateur.ModesPaiement.ESPECES:
            return "Espèces"
        elif self.mode_paiement == OperationUtilisateur.ModesPaiement.CHEQUE:
            return "Chèque"
        elif self.mode_paiement == OperationUtilisateur.ModesPaiement.VIREMENT:
            return "Virement"
        else:
            return "-"

    @staticmethod
    def get_or_404(operation_id):
        try:
            operation = OperationUtilisateur.objects.get(id=operation_id)
            return operation
        except OperationUtilisateur.DoesNotExist:
            raise Http404()
        
    # montant =
    # ferme =  [si None => argent dû à l'amap]
    # utilisateur =
    # date_enregistrement =
    # motif =   [lorsqu'un utilisateur a payé => motif = Paiement "XXXXX" enregistré par l'application via les interfaces des gestionnaires et paysans]
    # mode_paiement

    # un Select "Statut" pour savoir où ça en est
    # utilisateur_payé => permet à l'utilisateur de marquer le paiement comme déposé
    # amap confirmé => l'amap confirme que le paiement a été transmis aux fermes
    # reçu => la ferme confirme qu'elle a reçu le paiement


class Acompte(models.Model):
    # montant =
    # date_expiration =
    # utilisateur =
    # ferme = [si None => insamap]
    # motif éventuel ?
    pass


