from django.apps import AppConfig


class GaiaComptaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gaia_compta'
    verbose_name = "Gaia - Comptabilité"
