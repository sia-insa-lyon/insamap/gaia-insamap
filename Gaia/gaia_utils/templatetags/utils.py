from django.template.defaulttags import register
from gaia_users.models import Utilisateur
from django.template.loader import render_to_string
from datetime import datetime

@register.filter
def get_item(iterable, key):
    return iterable[key]

@register.filter
def times(value1, value2):
    return value1 * value2

@register.filter
def strcat(value1, value2):
    return str(value1) + str(value2)

@register.filter
def mini(value1, value2):
    return min(value1, value2)

@register.filter
def is_allowed(inscription, user):
    try:
        if not inscription.adhesion_requise or Utilisateur.is_adherent(user) or user.essai_gratuit:
            return True
        else:
            return False
    except:
        return False

@register.filter
def is_past(date):
    return date < datetime.now()

@register.simple_tag
def url_replace(request, field, value):

    dict_ = request.GET.copy()

    dict_[field] = value

    return dict_.urlencode()

@register.simple_tag
def pagination_widget(page_obj, request, page_name="page"):
    pagination_widget = "widgets/pagination_widget.html"
    return render_to_string(pagination_widget, {"page_obj": page_obj,"request": request, "page_name": page_name})