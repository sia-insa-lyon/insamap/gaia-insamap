from django.forms import DateInput, TimeInput, DateTimeInput


class GaiaDateInput(DateInput):
    input_type = "date"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if "class" in self.attrs.keys():
            self.attrs["class"] += " form-control"
        else:
            self.attrs["class"] = "form-control"


class GaiaTimeInput(TimeInput):
    input_type = "time"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if "class" in self.attrs.keys():
            self.attrs["class"] += " form-control"
        else:
            self.attrs["class"] = "form-control"


class GaiaDateTimeInput(DateTimeInput):
    input_type = "datetime-local"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if "class" in self.attrs.keys():
            self.attrs["class"] += " form-control"
        else:
            self.attrs["class"] = "form-control"
        if "placeholder" not in self.attrs.keys():
            self.attrs["placeholder"] = "jj/mm/aaaa HH:MM"
