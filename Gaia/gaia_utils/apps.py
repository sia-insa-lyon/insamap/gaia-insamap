from django.apps import AppConfig


class GaiaUtilsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gaia_utils'
    verbose_name = "Gaia - Autres"
