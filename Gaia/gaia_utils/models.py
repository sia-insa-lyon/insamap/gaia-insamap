from django.db import models
from django.http import Http404


class GaiaSetting(models.Model):
    key = models.CharField(verbose_name="Clé", null=False, blank=False, unique=True, max_length=256, editable=False,
                           primary_key=True)
    value = models.CharField(verbose_name="Valeur", max_length=256)

    @staticmethod
    def get_value(key):
        if key == "TARIF_ADHESION":
            return float(GaiaSetting.objects.get_or_create(key="TARIF_ADHESION", defaults={"value": "13"})[0].value)
        elif key == "DELAI_ARCHIVAGE_DISTRIBUTIONS":
            return int(GaiaSetting.objects.get_or_create(key="DELAI_ARCHIVAGE_DISTRIBUTIONS", defaults={"value": "10"})[0].value)
        elif key == "DELAI_ARCHIVAGE_CONTRATS":
            return int(GaiaSetting.objects.get_or_create(key="DELAI_ARCHIVAGE_CONTRATS", defaults={"value": "35"})[0].value)
        elif key == "QUANTITE_PRODUIT_MAX_UTILISATEUR":
            return int(GaiaSetting.objects.get_or_create(key="QUANTITE_PRODUIT_MAX_UTILISATEUR", defaults={"value": "30"})[0].value)
        else:
            try:
                setting = GaiaSetting.objects.get(key=key)
                return setting.value
            except GaiaSetting.DoesNotExist:
                return None
        
    def __str__(self):
        return self.key + " : " + self.value

class GaiaHypertext(models.Model):
    """
    Listes des fragments de texte hypertexte utilisés dans l'application.
    ACCUEIL_UTILISATEUR_INFOS : Page profil utilisateur
    ACCUEIL_SITE : Page d'accueil
    PAGE_NOUVEAU_CONTRAT : Page de création d'un nouveau contrat
    PAGE_SUCCES_CONTRAT : Page de succès de création d'un contrat
    PAGE_ADHERER : Page d'adhésion
    PAGE_MES_ADHESIONS : Page de liste des adhésions
    PAGE_MES_CONTRATS : Page de liste des contrats
    PAGE_MES_FERMES : Page de liste des fermes
    DASHBOARD : Tableau de bord gestionnaire
    """
    
    name = models.CharField(verbose_name="Identifiant", primary_key=True, max_length=50)
    active = models.BooleanField(verbose_name="Actif ?", default=False)
    display_name = models.CharField(verbose_name="Nom", null=False, blank=True, max_length=256)
    description = models.CharField(verbose_name="Description", null=False, blank=True, max_length=1024)
    content = models.TextField(verbose_name="Contenu", null=False, blank=True, default="<div class='alert alert-info'>Votre message d'information ici</div>")

    @staticmethod
    def find_content(fragment_name):
        try:
            fragment = GaiaHypertext.objects.get(name=fragment_name)
            if fragment.active:
                return fragment.content
            else:
                return ""
        except GaiaHypertext.DoesNotExist:
            return ""
        
    @staticmethod
    def get_or_404(fragment_id):
        try:
            fragment = GaiaHypertext.objects.get(name=fragment_id)
            return fragment
        except GaiaHypertext.DoesNotExist:
            raise Http404()
