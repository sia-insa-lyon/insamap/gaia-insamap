from gaia_distribs.models import InscriptionAdherent, InscriptionProduit, Distribution, Contrat
from gaia_paysans.models import Produit
from gaia_paysans.signals import update_archive_produit
from django.db.models.signals import post_save
from django.dispatch import receiver
from datetime import datetime


@receiver(post_save, sender=InscriptionAdherent)
def update_archive_inscription_adhrent(sender, instance, created, **kwargs):
    # Si la distribution n'est pas archivée et que le contrat est archivé, on le dé-archive
    if instance.distribution.archive == False and instance.contrat and instance.contrat.archive == True:
            instance.contrat.archive = False
            instance.contrat.save()


@receiver(post_save, sender=InscriptionProduit)
def update_archive_inscription_produit(sender, instance, created, **kwargs):
    # Si la distribution n'est pas archivée et que le produit est archivé, on le dé-archive
    if instance.distribution.archive == False and instance.produit and instance.produit.archive == True:
            instance.produit.archive = False
            instance.produit.save()
        
@receiver(post_save, sender=Distribution)
def update_archive_distribution(sender, instance, created, **kwargs):
    # Si la distribution est dé-archivée, on dé-archive les produits et contrats associés
    if instance.archive == False:
        produits_id = InscriptionProduit.objects.filter(distribution=instance).values_list('produit', flat=True).distinct()
        produits = Produit.objects.filter(id__in=produits_id, archive=True)
        
        for produit in produits:
            produit.archive = False
            produit.save()

        contrats_id = InscriptionAdherent.objects.filter(distribution=instance).values_list('contrat', flat=True).distinct()
        contrats = Contrat.objects.filter(id__in=contrats_id, archive=True)
        
        for contrat in contrats:
            contrat.archive = False
            contrat.save()

    