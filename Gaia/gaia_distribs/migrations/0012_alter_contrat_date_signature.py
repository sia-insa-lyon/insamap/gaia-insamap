# Generated by Django 3.2.7 on 2024-04-17 14:25

from django.db import migrations, models

def update_date_modification(apps, schema_editor):
    Contrat = apps.get_model('gaia_distribs', 'Contrat')
    Contrat.objects.filter(date_signature__isnull=False, date_modification__isnull=True).update(date_modification=models.F('date_signature'))

class Migration(migrations.Migration):

    dependencies = [
        ('gaia_distribs', '0011_alter_inscriptionadherent_unites_commandees'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contrat',
            name='date_signature',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.RunPython(update_date_modification, migrations.RunPython.noop),
    ]
