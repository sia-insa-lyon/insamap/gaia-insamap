# Generated by Django 3.2.7 on 2024-03-18 22:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gaia_distribs', '0007_auto_20240308_2010'),
    ]

    operations = [
        migrations.AlterField(
            model_name='inscriptionproduit',
            name='adhesion_requise',
            field=models.BooleanField(default=True, verbose_name='Adhésion requise pour livraison unique ?'),
        ),
    ]
