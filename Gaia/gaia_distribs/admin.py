from django.contrib import admin
from datetime import datetime

from django.urls import reverse
from gaia_distribs.models import Distribution, InscriptionProduit, InscriptionAdherent, Contrat
from gaia_paysans.models import Produit, Ferme
from django.db import models
from django.db.models import Q, F
from django.forms import TextInput
from django import forms

from datetime import datetime
from django.utils import timezone

class DateDistributionFilter(admin.SimpleListFilter):
    title = "chronologie"
    parameter_name = "chronologie"

    def lookups(self, request, model_admin):
        return [
            ("future", "Futures"),
            ("past", "Passées")
        ]

    def queryset(self, request, queryset):
        if self.value() == "future":
            return queryset.filter(date_heure__gte=(datetime.now()-F('duree')))
        if self.value() == "past":
            return queryset.filter(date_heure__lt=(datetime.now()-F('duree')))
        else:
            return queryset

class DateVisibiliteFilter(admin.SimpleListFilter):
    title = "visibilite"
    parameter_name = "visibilite"

    def lookups(self, request, model_admin):
        return [
            ("not_visible", "Non visible"),
            ("paysans_visible", "Visible par les paysans"),
            ("users_visible", "Visible par les utilisateurs"),
            ("all_visible", "Visible par tous")
        ]

    def queryset(self, request, queryset):
        if self.value() == "not_visible":
            return queryset.filter(date_visibilite_paysans__gt=timezone.now()).filter(date_visibilite_utilisateurs__gt=timezone.now())
        if self.value() == "paysans_visible":
            return queryset.filter(Q(date_visibilite_paysans__exact=None)|Q(date_visibilite_paysans__lte=timezone.now()))
        if self.value() == "users_visible":
            return queryset.filter(Q(date_visibilite_utilisateurs__exact=None)|Q(date_visibilite_utilisateurs__lte=timezone.now()))
        if self.value() == "all_visible":
            return queryset.filter(Q(date_visibilite_paysans__exact=None)|Q(date_visibilite_paysans__lte=timezone.now())).filter(Q(date_visibilite_utilisateurs__exact=None)|Q(date_visibilite_utilisateurs__lte=timezone.now()))
        else:
            return queryset
        
class ContratStatusFilter(admin.SimpleListFilter):
    title = "statut du contrat"
    parameter_name = "contrat"

    def lookups(self, request, model_admin):
        return [
            (None, "Contrats signés"),
            ("notsigned", "Contrats en cours de création"),
            ("all", "Tous")
        ]
    
    def choices(self, cl):
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == lookup,
                'query_string': cl.get_query_string({
                    self.parameter_name: lookup,
                }, []),
                'display': title,
            }

    def filter_for_InscriptionAdherent(self, queryset):
        if self.value() == "notsigned":
            return queryset.filter(contrat__date_signature__isnull=True)
        if self.value() == None:
            return queryset.filter(contrat__date_signature__isnull=False)
        else:
            return queryset
        
    def filter_for_Contrat(self, queryset):
        if self.value() == "notsigned":
            return queryset.filter(date_signature__isnull=True)
        if self.value() == None:
            return queryset.filter(date_signature__isnull=False)
        else:
            return queryset
    
    def queryset(self, request, queryset):
        if queryset.model == InscriptionAdherent:
            return self.filter_for_InscriptionAdherent(queryset)
        if queryset.model == Contrat:
            return self.filter_for_Contrat(queryset)
        else:
            return queryset   

class ArchiveFilter(admin.SimpleListFilter):
    title = "archivage"
    parameter_name = "archive"
    
    def lookups(self, request, model_admin):
        texte = "archivé"
        if model_admin.model == Distribution or model_admin.model == Ferme:
            texte = "archivée"
            
        return [
            (None, "Non " + texte),
            ("archive", texte.capitalize()),
            ("all", "Tous")
        ]
    
    def choices(self, cl):
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == lookup,
                'query_string': cl.get_query_string({
                    self.parameter_name: lookup,
                }, []),
                'display': title,
            }

    def filter_for_InscriptionAdherent(self, queryset):
        if self.value() == "archive":
                return queryset.filter(distribution__archive=True)
        if self.value() == None:
            return queryset.filter(distribution__archive=False)
        else:
            return queryset
    
    def filter_for_InscriptionProduit(self, queryset):
        if self.value() == "archive":
                return queryset.filter(distribution__archive=True)
        if self.value() == None:
            return queryset.filter(distribution__archive=False)
        else:
            return queryset
    
    def queryset(self, request, queryset):
        if queryset.model == InscriptionAdherent:
            return self.filter_for_InscriptionAdherent(queryset)
        elif queryset.model == InscriptionProduit:
            return self.filter_for_InscriptionProduit(queryset)
        else:
            if self.value() == "archive":
                return queryset.filter(archive=True)
            if self.value() == None:
                return queryset.filter(archive=False)
            else:
                return queryset

class InscriptionProduitInlineForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance and hasattr(self.instance, 'produit') and Produit.Frequences.UNIQUE not in self.instance.produit.frequences:
            if 'livraison_unique' in self.fields:
                self.fields['livraison_unique'].disabled = True
            if 'adhesion_requise' in self.fields:
                self.fields['adhesion_requise'].disabled = True

class InscriptionProduitInline(admin.TabularInline):
    model = InscriptionProduit
    extra = 0
    form = InscriptionProduitInlineForm
    ordering = ("-distribution__date_heure", "produit__ferme__nom", "produit__nom",)
    autocomplete_fields = ('produit', 'distribution',)
    
    def formfield_for_dbfield(self, db_field, **kwargs):
        formfield = super().formfield_for_dbfield(db_field, **kwargs)
        if db_field.name in ['tarif_vente', 'unites_dispo', 'unites_restantes', 'delai_max_fermeture_inscriptions']:
            formfield.widget.attrs['style'] = 'width: 4em;'
        elif db_field.name == 'quantite_unitaire':
            formfield.widget.attrs['style'] = 'width: 7em;'
        return formfield 
    
    def get_queryset(self, request):
        # Si la recherche est effectuée depuis la page d'une distribution, on ne filtre pas car si la distribution est archivée, on veut voir les inscriptions
        if "/gaia_distribs/distribution/" in request.path:
            return super().get_queryset(request)
        else:
            # Si la recherche est effectuée depuis la page d'un produit, on ne veut voir que les inscriptions des distributions non archivées
            return super().get_queryset(request).filter(distribution__archive=False)
            
    
class InscriptionAdherentInline(admin.TabularInline):
    model = InscriptionAdherent
    extra = 0
    fields = ('unites_commandees', 'inscription_produit', 'produit', 'distribution', 'recupere',)
    ordering = ("-distribution__date_heure", "produit__ferme__nom", "produit__nom",)
    autocomplete_fields = ("inscription_produit",)
    readonly_fields = ('produit', 'distribution',)

@admin.register(Distribution)
class DistributionAdminPanel(admin.ModelAdmin):
    actions = ["make_visible_by_all", "delete_selected"]
    date_hierarchy = "date_heure"
    fieldsets = (
        ("Date et Lieu", {
            'fields': ('date_heure', 'duree', 'lieu',)
        }),
        ('Visibilité', {
            'fields': ('date_visibilite_paysans', 'date_visibilite_utilisateurs',),
        }),
        ("Statut", {
            "fields": ("archive",),
            "classes": ("collapse",)
        })
    )
    search_fields = ("date_heure",)
    inlines = (InscriptionProduitInline,)
    list_display = ("date_heure", "duree", "get_nb_inscriptions_produit",)
    list_display_links = ("date_heure", "duree",)
    list_filter = (ArchiveFilter, DateDistributionFilter, DateVisibiliteFilter,)
    ordering = ("-date_heure",)
    show_full_result_count = False
    
    def get_search_results(self, request, queryset, search_term):
        queryset, use_distinct = super().get_search_results(request, queryset, search_term)
        try:
            queryset |= self.model.objects.filter(date_heure__date=datetime.strptime(search_term, '%d/%m/%y').date())
        except ValueError:
            try:
                queryset |= self.model.objects.filter(date_heure__date=datetime.strptime(search_term, '%d/%m/%Y').date())
            except ValueError:
                pass
            
        # Si la recherche n'est pas effectuée depuis la page d'administration des distributions
        if not request.path.endswith("/gaia_distribs/distribution/"):
            queryset = queryset.filter(archive=False)
        queryset = queryset.order_by("-date_heure")
        return queryset, use_distinct
    
    @admin.action(description="Rendre visible pour tous")
    def make_visible_by_all(self, request, queryset):
        queryset.filter(date_visibilite_paysans__gte=datetime.now()).update(date_visibilite_paysans=datetime.now())
        queryset.filter(date_visibilite_utilisateurs__gte=datetime.now()).update(date_visibilite_utilisateurs=datetime.now())
        self.message_user(request, "Les distributions sélectionnées sont maintenant visibles pour tous.")

    def view_on_site(self, obj):
        return reverse("delivery_details_admin", args=[obj.id])
    
@admin.register(InscriptionProduit)
class InscriptionProduitAdminPanel(admin.ModelAdmin):
    actions = ("sign_amap", "sign_farm", "delete_selected",)
    date_hierarchy = "distribution__date_heure"
    fieldsets = [
        (None, {
            'fields': ('produit', 'distribution', 'unites_dispo', 'delai_max_fermeture_inscriptions',)
        }),
        ('Signatures', {
            'fields': ('signature_amap', 'signature_ferme',),
        }),
        ('Tarifs et quantités', {
            'fields': ('tarif_vente', 'quantite_unitaire', 'unites_restantes',),
            'classes': ('collapse',)
        }),
    ]
    list_display = ("produit", "distribution", "_all_signatures",)
    list_display_links = ("produit", "distribution",)
    list_filter = (ArchiveFilter, 'livraison_unique',)
    autocomplete_fields = ('produit', 'distribution',)
    search_fields = ("produit__nom",)
    show_full_result_count = False

    def get_search_results(self, request, queryset, search_term):
        queryset, use_distinct = super().get_search_results(request, queryset, search_term)
        try:
            queryset |= self.model.objects.filter(distribution__date_heure__date=datetime.strptime(search_term, '%d/%m/%y').date())
        except ValueError:
            try:
                queryset |= self.model.objects.filter(distribution__date_heure__date=datetime.strptime(search_term, '%d/%m/%Y').date())
            except ValueError:
                pass
        queryset = queryset.order_by("-distribution__date_heure")
        # Si la recherche n'est pas effectuée depuis la page d'administration des inscriptions produits
        if not request.path.endswith("/gaia_distribs/inscriptionproduit/"):
            queryset = queryset.filter(distribution__archive=False)
        return queryset, use_distinct
    
    def get_fieldsets(self, request, obj=None):
        fieldsets = self.fieldsets.copy()
        if not obj or (obj.produit and Produit.Frequences.UNIQUE in obj.produit.frequences):
            fieldsets.insert(2, ('Livraison Unique', {'fields': ('livraison_unique', 'adhesion_requise',)}))
        else:
            fieldsets.insert(2, ("Ce produit n'est pas disponible en livraison unique", {'fields': ()}))
        
        return fieldsets
       
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "produit":
            kwargs["queryset"] = Produit.objects.order_by("ferme__nom", "nom")
        elif db_field.name == "distribution":
            kwargs["queryset"] = Distribution.objects.order_by("-date_heure")
        return super(InscriptionProduitAdminPanel, self).formfield_for_foreignkey(db_field, request, **kwargs)
    
    @admin.action(description="Marquer signées par l'Amap")
    def sign_amap(self, request, queryset):
        queryset.update(signature_amap=True)
        self.message_user(request, "Les inscriptions sélectionnées sont maintenant signées par l'Amap.")

    @admin.action(description="Marquer signées par la ferme")
    def sign_farm(self, request, queryset):
        queryset.update(signature_ferme=True)
        self.message_user(request, "Les inscriptions sélectionnées sont maintenant signées par la ferme.")

    def view_on_site(self, obj):
        return reverse("product_registration_details_admin", args=[obj.id])

@admin.register(InscriptionAdherent)
class InscriptionAdherentAdminPanel(admin.ModelAdmin):
    date_hierarchy = "distribution__date_heure"
    fields = ("contrat", "utilisateur", "inscription_produit", "produit", "distribution", "unites_commandees", "recupere",)
    list_display = ("utilisateur", "produit", "distribution", "unites_commandees",)  
    list_display_links = ("utilisateur", "produit", "distribution",)
    list_filter = (ArchiveFilter, ContratStatusFilter, "recupere",)
    autocomplete_fields = ("utilisateur", "produit", "distribution","inscription_produit", "contrat",)
    search_fields = ("utilisateur__last_name", "utilisateur__first_name", "produit__nom",)
    readonly_fields = ("utilisateur", "produit", "distribution",)
    show_full_result_count = False

    def get_search_results(self, request, queryset, search_term):
        queryset, use_distinct = super().get_search_results(request, queryset, search_term)
        try:
            queryset |= self.model.objects.filter(distribution__date_heure__date=datetime.strptime(search_term, '%d/%m/%y').date())
        except ValueError:
            try:
                queryset |= self.model.objects.filter(distribution__date_heure__date=datetime.strptime(search_term, '%d/%m/%Y').date())
            except ValueError:
                pass
        queryset = queryset.order_by("-distribution__date_heure")
        # Si la recherche n'est pas effectuée depuis la page d'administration des inscriptions adhérents
        if not request.path.endswith("/gaia_distribs/inscriptionadherent/"):
            queryset = queryset.filter(distribution__archive=False)
        return queryset, use_distinct
    

@admin.register(Contrat)
class ContratAdminPanel(admin.ModelAdmin):
    actions = ("delete_non_signed_contracts",)
    date_hierarchy = "date_signature"
    fieldsets = (
        (None, {
            "fields": ("souscripteur", "ferme", "date_creation", "date_signature", "date_modification", "numero_groupe",)
        })
        ,
        ("Statut", {
            "fields": ("archive",),
            "classes": ("collapse",)
        }))
    inlines = (InscriptionAdherentInline,)
    list_display = ("id", "souscripteur", "ferme", "statut", "nombre_livraisons")
    list_display_links = ("id", "souscripteur", "ferme")
    list_filter = (ArchiveFilter, ContratStatusFilter, ("ferme", admin.RelatedFieldListFilter),)
    autocomplete_fields = ("souscripteur", "ferme",)
    search_fields = ("id__iexact", "souscripteur__last_name", "souscripteur__first_name",)
    readonly_fields = ("date_creation", "numero_groupe",)
    show_full_result_count = False
    
    def view_on_site(self, obj):
        return reverse("contrat_details", args=[obj.id])
    
    def get_search_results(self, request, queryset, search_term):
        queryset, use_distinct = super().get_search_results(request, queryset, search_term)
        # Si la recherche n'est pas effectuée depuis la page d'administration des contrats
        if not request.path.endswith("/gaia_distribs/contrat/"):
            queryset = queryset.filter(archive=False)
        return queryset, use_distinct
    
    def get_readonly_fields(self, request, obj=None):
        readonly_fields =["date_creation", "numero_groupe", "date_modification"]
        # On doit vérifier la signature du contrat avec la version enregistrée en base de données, pas celle en cours de modification
        if obj and Contrat.objects.get(id=obj.id).date_signature is not None: 
            readonly_fields.append("date_signature")
        return readonly_fields


    
