import logging
from datetime import datetime
from io import BytesIO

from dateutil.relativedelta import relativedelta
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db import IntegrityError, transaction
from django.db.models import Q
from django.http import HttpResponse, HttpResponseNotFound, Http404
from django.shortcuts import render, redirect
from django.template.loader import get_template
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic.list import ListView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from xhtml2pdf import pisa

from Gaia import settings
from gaia_compta.models import OperationUtilisateur
from gaia_distribs.forms import FastDistributionCreationForm, ProductsRegistrationsForm, TimeUnitChoices, RegisterAdherentDistribsForm, \
    FinalisationInscriptionDistributionsForm, EmargementForm
from gaia_distribs.models import LastCreatedDistributions, Distribution, Contrat, InscriptionProduit, \
    InscriptionAdherent
from gaia_paysans.models import Produit,Ferme
from gaia_users.models import Adhesion, Utilisateur
from gaia_utils.models import GaiaHypertext, GaiaSetting


class RegisterMultipleDistributionsView(PermissionRequiredMixin, View):
    template_name = "distribs/multiple_create.html"
    permission_required = "gaia_distribs.add_distribution"
    raise_exception = True

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.is_gestionnaire, login_url="profile_home", redirect_field_name=None
    ))
    def get(self, request):
        form = FastDistributionCreationForm()
        logging.getLogger(__name__).debug("GET")

        return render(request, self.template_name, {
            "form": form
        })

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.is_gestionnaire, login_url="profile_home", redirect_field_name=None
    ))
    def post(self, request):
        form = FastDistributionCreationForm(request.POST)
        logging.getLogger(__name__).debug("POST")

        if form.is_valid():
            start = form.cleaned_data["date_from"]
            end = form.cleaned_data["date_to"]
            delta = form.cleaned_data["time_delta"]
            unit = int(form.cleaned_data["delta_unit"])

            hour_start = form.cleaned_data["heure_debut"]
            hour_end = form.cleaned_data["heure_fin"]
            place = form.cleaned_data["lieu"]
            paysans_visibility = form.cleaned_data["date_visibilite_paysans"]
            utilisateurs_visibility = form.cleaned_data["date_visibilite_utilisateurs"]
            if hour_end < hour_start:
                end_date = datetime.combine(start.replace(day=start.day + 1), hour_end)
                delta = end_date - start
                length = (datetime.min + delta).time()
            else:
                length = (datetime.min +
                          (datetime.combine(start, hour_end) - datetime.combine(start, hour_start))
                          ).time()

            dates = self.compute_dates(start, end, delta, unit)
            added_dates = []

            LastCreatedDistributions.objects.all().delete()
            last_date = dates[0] if len(dates) > 0 else None
            try:
                with transaction.atomic():
                    for date in dates:
                        last_date = date
                        distribution = Distribution(
                            date_heure=datetime.combine(date, hour_start),
                            duree=length,
                            lieu=place,
                            date_visibilite_paysans=paysans_visibility,
                            date_visibilite_utilisateurs=utilisateurs_visibility
                        )
                        distribution.save()
                        added_dates.append(date)

            except IntegrityError:
                form.add_error(
                    None,
                    "Impossible de créer la distribution pour la date %s : violation de contrainte d'intégrité "
                    "(la distribution existe probablement déjà)" % last_date.strftime("%d/%m/%Y")
                )
                return render(request, self.template_name, {
                    "form": form
                })

            form = FastDistributionCreationForm()

            return render(request, self.template_name, {
                "form": form,
                "added_dates": added_dates
            })

        return render(request, self.template_name, {
            "form": form
        })

    def compute_dates(self, start, end, delta, delta_unit):
        time_delta = None
        dates = []

        if delta_unit == TimeUnitChoices.DAY.value:
            time_delta = relativedelta(days=delta)
        elif delta_unit == TimeUnitChoices.WEEK.value:
            time_delta = relativedelta(days=7 * delta)
        elif delta_unit == TimeUnitChoices.MONTH.value:
            time_delta = relativedelta(months=delta)
        elif delta_unit == TimeUnitChoices.YEAR.value:
            time_delta = relativedelta(years=delta)
        else:
            raise Exception(
                "Unknown specified time delta. This exception being thrown means that form verification failed somewhere.")

        while start <= end:
            dates.append(start)
            start += time_delta

        return dates

class ProductsRegistrationsAdminView(View):
    template_name = "distribs/products_registrations.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_gestionnaire, login_url="profile_home", redirect_field_name=None))
    def get(self, request):
        distributions = Distribution.objects.filter(date_heure__gte=datetime.now()).order_by("date_heure")
        produits = Produit.objects.filter(archive=False).order_by("nom")
        form = ProductsRegistrationsForm(distributions, produits)
        return render(request, self.template_name, {'form': form})
    
    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_gestionnaire, login_url="profile_home", redirect_field_name=None))
    def post(self, request):
        distributions = Distribution.objects.filter(date_heure__gte=datetime.now()).order_by("date_heure")
        produits = Produit.objects.filter(archive=False).order_by("nom")
        form = ProductsRegistrationsForm(distributions, produits, request.POST)
        if form.is_valid():
            liste_distributions = []
            for distribution in form.distributions:
                if form.cleaned_data["distribution_" + str(distribution.id)]:
                    liste_distributions.append(distribution)
            
            liste_produits = []
            for produit in form.produits:
                dispo = form.cleaned_data["produit_" + str(produit.id) + "_unites_dispo"]
                delai = form.cleaned_data["produit_" + str(produit.id) + "_delai_max"]
                if form.cleaned_data["produit_" + str(produit.id)]:
                    liste_produits.append((produit, dispo, delai))
                    if not(dispo and dispo > 0):
                        form.add_error(
                            None,
                            "Veuillez renseigner le nombre d'unités disponibles pour le produit : " + produit.nom + " (" + produit.ferme.nom + ")"
                        )
                    if not(delai and delai > 0):
                        form.add_error(
                        None,
                        "Veuillez renseigner le délai de commande maximal pour le produit : " + produit.nom + " (" + produit.ferme.nom + ")"
                    )
                        
                    
            if len(liste_distributions) == 0:
                form.add_error(
                    None,
                    "Veuillez sélectionner au moins une distribution."
                )
            if len(liste_produits) == 0:
                form.add_error(
                    None,
                    "Veuillez sélectionner au moins un produit."
                )

            if not form.errors:
                compteur = 0
                for distribution in liste_distributions:
                    for produit, dispo, delai in liste_produits:
                        try:
                            inscription = InscriptionProduit(
                                distribution=distribution,
                                produit=produit,
                                unites_dispo=dispo,
                                delai_max_fermeture_inscriptions=delai,
                                signature_amap=form.cleaned_data["signature_amap"],
                                signature_ferme=form.cleaned_data["signature_ferme"]
                            )
                            inscription.save()
                            compteur += 1
                        except IntegrityError:
                            form.add_error(
                                None,
                                "Impossible de créer l'inscription pour le produit {0} ({1})à la distribution du {2} (l'inscription existe probablement déjà)".format(produit.nom, produit.ferme.nom, distribution.date_heure.strftime("%d/%m/%Y"))
                            )
                
                return render(request, self.template_name, {'form': form, 'success': compteur})

        return render(request, self.template_name, {'form': form})

class MyContratsView(View):
    template_name = "distribs/contrat/my_contrats.html"
    paginate_by = 25

    @method_decorator(login_required)
    def get(self, request):
        contrats = Contrat.objects.filter(
            Q(souscripteur__exact=request.user)
            & Q(date_signature__isnull=False)
        ).order_by("-date_signature", "-id")

        active_contrats = []
        expired_contrats = []
        for contrat in contrats:
            if contrat.date_fin() < datetime.now():
                expired_contrats.append(contrat)
            else:
                active_contrats.append(contrat)

        active_paginator = Paginator(active_contrats, self.paginate_by)
        expired_paginator = Paginator(expired_contrats, self.paginate_by)

        active_page_number = self.request.GET.get('active_page')
        expired_page_number = self.request.GET.get('expired_page')

        try:
            active_page = active_paginator.page(active_page_number)
        except PageNotAnInteger:
            active_page = active_paginator.page(1)
        except EmptyPage:
            active_page = active_paginator.page(active_paginator.num_pages)

        try:
            expired_page = expired_paginator.page(expired_page_number)
        except PageNotAnInteger:
            expired_page = expired_paginator.page(1)
        except EmptyPage:
            expired_page = expired_paginator.page(expired_paginator.num_pages)

        fragment = GaiaHypertext.find_content("PAGE_MES_CONTRATS")

        return render(request, self.template_name, {
            "expired_contrats": expired_page,
            "active_contrats": active_page,
            "PAGE_MES_CONTRATS": fragment
        })

class RegisterInscriptionAdherentProduitView(View):
    # TODO une vue identique mais qui prend un contrat_id en paramètre et remplit le formulaire avec
    # et lorsque POST, elle fait la différence et modifie
    # TODO limiter le nombre de paniers commandables (par un utilisateur lambda, les gestionnaires ont tous les droits si besoin
    template_name = "distribs/inscription/register_adherent_distribution.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.is_legitimate, login_url="email_not_confirmed_page", redirect_field_name=None
    ))
    def get(self, request, distrib_unique=None):

        distributions, produits_par_distribution = self.get_distribs_and_produits(distrib_unique=distrib_unique)

        fermes, produits_par_ferme, inscriptions_par_produit = self.get_fermes_produits_par_fermes_and_inscriptions(distrib_unique=distrib_unique)

        form = RegisterAdherentDistribsForm(distributions, produits_par_distribution)

        est_adherent = Adhesion.objects.filter(annee=datetime.now().year).filter(
                utilisateur_id__exact=request.user.id
        ).count() > 0

        distributions_par_frequences_par_produit = self.get_distributions_par_frequences_par_produit(inscriptions_par_produit)

        fragment = GaiaHypertext.find_content("PAGE_NOUVEAU_CONTRAT")

        return render(request, self.template_name, {
            "form": form,
            "distributions": distributions,
            "fermes": fermes,
            "produits_par_ferme": produits_par_ferme,
            "inscriptions_par_produit": inscriptions_par_produit,
            "distributions_par_frequences_par_produit": distributions_par_frequences_par_produit,
            "commande_max": GaiaSetting.get_value("QUANTITE_PRODUIT_MAX_UTILISATEUR"),
            "essai_gratuit": request.user.essai_gratuit,
            "is_adherent": est_adherent,
            "distrib_unique": distrib_unique,
            "PAGE_NOUVEAU_CONTRAT": fragment
        })

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.is_legitimate, login_url="email_not_confirmed_page", redirect_field_name=None
    ))
    def post(self, request, distrib_unique=None):
        distributions, produits_par_distribution = self.get_distribs_and_produits(include_if_unite_restante_zero=True, distrib_unique=distrib_unique)
        # Pour la création du formulaire, on doit aussi inclure les produits dont les unités restantes sont à 0
        # car l'utilisateur peut vouloir s'inscrire à une distribution dont le nombre d'unités restantes
        # est devenu nul entre le moment où il a chargé la page et le moment où il a soumis le formulaire

        form = RegisterAdherentDistribsForm(distributions, produits_par_distribution, request.POST)

        if form.is_valid():
            try:
                id_groupe = -1
                with transaction.atomic():
                    id_groupe = self.process_form_submission(
                        form=form,
                        request=request,
                        distributions=distributions,
                        produits_par_distribution=produits_par_distribution
                    )

                return redirect(to="recap_reglements_inscription", id_groupe=id_groupe)
            except Produit.DoesNotExist:
                form.add_error(
                    None, "Un des produits demandés n'existe pas ou plus."
                )
            except Distribution.DoesNotExist:
                form.add_error(None, "Une des distributions demandées n'existe pas ou plus.")
            except ValueError as e:
                form.add_error(None, str(e))
            except Exception as e:
                form.add_error(
                    None,
                    "Une erreur inconnue est survenue lors de l'enregistrement (heure: {0}). "
                    "Contactez-nous si l'erreur se reproduit.".format(datetime.now())
                )
                logging.getLogger("InscriptionDistribution").critical(
                    "Une erreur est survenue lors d'une tentative d'inscription : " + str(e)
                )

        fermes, produits_par_ferme, inscriptions_par_produit = self.get_fermes_produits_par_fermes_and_inscriptions(distrib_unique=distrib_unique)
        
        est_adherent = Adhesion.objects.filter(annee=datetime.now().year).filter(
                utilisateur_id__exact=request.user.id
        ).count() > 0

        distributions_par_frequences_par_produit = self.get_distributions_par_frequences_par_produit(inscriptions_par_produit)

        fragment = GaiaHypertext.find_content("PAGE_NOUVEAU_CONTRAT")
        
        return render(request, self.template_name, {
            "form": form,
            "distributions": distributions,
            "fermes": fermes,
            "produits_par_ferme": produits_par_ferme,
            "inscriptions_par_produit": inscriptions_par_produit,
            "distributions_par_frequences_par_produit": distributions_par_frequences_par_produit,
            "commande_max": GaiaSetting.get_value("QUANTITE_PRODUIT_MAX_UTILISATEUR"),
            "essai_gratuit": request.user.essai_gratuit,
            "is_adherent": est_adherent,
            "distrib_unique": distrib_unique,
            "PAGE_NOUVEAU_CONTRAT": fragment
        })

    def get_distribs_and_produits(self, include_if_unite_restante_zero=False, distrib_unique=None):
        """
        Récupération de la liste des créneaux de livraison disponibles, et des produits disponibles à ceux-ci.
        Utilisé pour la validation des données, par le formulaire de soumission de contrat.

        :return: un tuple [ list<Distribution>, map<Distribution.id, list<InscriptionProduit>> ]
        """
        distributions = Distribution.objects.filter(
            date_heure__gte=datetime.now()
        ).filter(
            Q(date_visibilite_utilisateurs__exact=None) | Q(date_visibilite_utilisateurs__lte=datetime.now())
        ).order_by("date_heure")

        if distrib_unique is not None:
            distributions = distributions.filter(id__exact=distrib_unique.id)

        produits_par_distribution = {}
        for distrib in distributions:
            produits = InscriptionProduit.objects.filter(
                distribution_id__exact=distrib.id
            ).filter(
                delai_max_fermeture_inscriptions__lt=(distrib.date_heure.date() - datetime.today().date()).days
            ).filter(
                Q(signature_amap=True) & Q(signature_ferme=True)
            )
            if include_if_unite_restante_zero:
                produits = produits.filter(unites_restantes__gte=0)
            else:
                produits = produits.filter(unites_restantes__gt=0)

            produits_par_distribution[distrib.id] = produits

        return distributions, produits_par_distribution

    def get_fermes_produits_par_fermes_and_inscriptions(self, distrib_unique=None):
        """
        Permet de récupérer la liste des fermes, avec leurs produits et les créneaux disponibles pour les commander.
        Utilisé uniquement pour l'affichage de la page de soumission de contrat.

        :return: un tuple [ list<Ferme>, dict<Ferme.id, list<Produit>>, dict<Produit.id, list<InscriptionProduit>> ]
        """
        distributions = Distribution.objects.filter(
            date_heure__gte=datetime.now()
        ).filter(
            Q(date_visibilite_utilisateurs__exact=None) | Q(date_visibilite_utilisateurs__lte=datetime.now())
        ).order_by("date_heure")

        if distrib_unique is not None:
            distributions = distributions.filter(id__exact=distrib_unique.id)

        inscriptions = []
        for distrib in distributions:
            ins = InscriptionProduit.objects.filter(
                distribution_id__exact=distrib.id
            ).filter(
                unites_restantes__gt=0
            ).filter(
                delai_max_fermeture_inscriptions__lt=(distrib.date_heure.date() - datetime.today().date()).days
            ).filter(
                Q(signature_amap=True) & Q(signature_ferme=True)
            ).order_by("produit__nom")

            if distrib_unique is not None:
                ins = ins.filter(livraison_unique=True)
                ins = [x for x in ins if Produit.Frequences.UNIQUE in x.produit.frequences]

            inscriptions.extend(ins)

        fermes = []
        produits_par_ferme = dict()
        inscriptions_par_produit = dict()
        for inscription in inscriptions:
            ferme = inscription.produit.ferme
            if ferme not in fermes:
                fermes.append(ferme)
                produits_par_ferme[ferme.id] = []

            produit = inscription.produit
            if produit not in produits_par_ferme[ferme.id]:
                produits_par_ferme[ferme.id].append(produit)
                inscriptions_par_produit[produit.id] = []

            inscriptions_par_produit[produit.id].append(inscription)

        return fermes, produits_par_ferme, inscriptions_par_produit

    def get_distributions_par_frequences_par_produit(self, inscriptions_par_produit):
        distributions_par_frequences_par_produit = {}

        for produit in inscriptions_par_produit.keys():
            distributions_par_frequences_par_produit[produit] = {}

            for frequence in Produit.Frequences.choices:
                    distributions_par_frequences_par_produit[produit][frequence[0]] = []

            objet_produit = Produit.objects.filter(id__exact=produit).all()

            if len(objet_produit) == 1:
                frequences = objet_produit[0].frequences
                compteur = 1

                for inscription in inscriptions_par_produit[produit]:

                    if Produit.Frequences.HEBDOMADAIRE_SEMESTRE in frequences:
                        distributions_par_frequences_par_produit[produit][Produit.Frequences.HEBDOMADAIRE_SEMESTRE].append(inscription.id)
                    
                    if Produit.Frequences.HEBDOMADAIRE_4 in frequences and compteur <=4:
                        distributions_par_frequences_par_produit[produit][Produit.Frequences.HEBDOMADAIRE_4].append(inscription.id)
                    
                    if Produit.Frequences.BIMENSUELLE_SEMESTRE in frequences and compteur % 2 == 1:
                        distributions_par_frequences_par_produit[produit][Produit.Frequences.BIMENSUELLE_SEMESTRE].append(inscription.id)
                    
                    if Produit.Frequences.BIMENSUELLE_4 in frequences and compteur % 2 == 1 and compteur < 8:
                        distributions_par_frequences_par_produit[produit][Produit.Frequences.BIMENSUELLE_4].append(inscription.id)

                    if Produit.Frequences.UNIQUE in frequences and inscription.livraison_unique:
                        distributions_par_frequences_par_produit[produit][Produit.Frequences.UNIQUE].append(inscription.id)
                    
                    compteur += 1
                    
        return distributions_par_frequences_par_produit
    
    def process_form_submission(self, form, request, distributions, produits_par_distribution):
        """
        Endpoint de soumission de contrat (validation et enregistrement des données)
        """
        contrats = dict()  # ferme.id => contrat
        numero_groupe = Contrat.next_numero_groupe(request.user.id)

        # extraction des quantitées par produit pour chaque distrib, et tentative d'enregistrement
        for field in form.cleaned_data:
            if form.cleaned_data[field] is None:  # ignore empty fields
                continue

            distrib_id = int(field.split("distribution_")[1])
            distrib = distributions.get(id=distrib_id)  # throws Distribution.DoesNotExist
            annee_adhesion = distrib.date_heure.year if distrib.date_heure.month >= 2 else distrib.date_heure.year - 1
            adherent = Adhesion.objects.filter(
                Q(annee__exact=annee_adhesion)
                & Q(utilisateur=request.user)
            ).count() > 0
            produits_inscrits = produits_par_distribution[distrib_id]

            produits_compressed = form.cleaned_data[field].split(';')
            produits_quantite = self.extract_produits_quantites(produits_compressed)

            self.try_register_inscription_adherent(
                distribution=distrib,
                inscriptions_produits=produits_inscrits,
                produits_quantite=produits_quantite,
                utilisateur=request.user,
                new_contrats_list=contrats,
                group_number=numero_groupe,
                is_adherent=adherent
            )

        return numero_groupe

    def extract_produits_quantites(self, produits_compressed):
        """
        Transforme une liste compressée de produits [ "Produit.id=quantite", ... ] en des tuples (Produit, int)
        """
        produits = []

        for couple in produits_compressed:
            id_prod, amount = couple.split("=")
            id_prod, amount = int(id_prod), int(amount)

            if amount == 0:
                continue

            produit = Produit.objects.get(id=id_prod)  # throws Produit.DoesNotExist
            produits.append((produit, amount), )

        return produits

    def try_register_inscription_adherent(self, distribution, inscriptions_produits, produits_quantite, utilisateur,
                                          new_contrats_list, group_number, is_adherent):
        for produit, qt in produits_quantite:
            inscription_produit = None

            for inscription in inscriptions_produits:
                if inscription.produit.id == produit.id:
                    inscription_produit = inscription
                    break

            if inscription_produit is None or inscription_produit.unites_restantes == 0:
                raise ValueError("Le produit {0} n'est plus disponible pour la distribution {1}.".format(
                    produit.nom, distribution.date_heure.strftime("%d/%m/%Y")
                ))

            # La vérification d'accès aux distributions éligibles présente auparavant ici est maintenant gérée par le front-end

            if inscription_produit.adhesion_requise and not is_adherent and not utilisateur.essai_gratuit:
                # TODO: gérer acomptes (pour paniers reportés type covid)
                raise ValueError(
                    "Vous avez souhaité vous inscrire pour une distribution en {0} (hors mois de janvier et période d'essai), "
                    "mais vous n'êtes pas adhérent.e sur cette année ! {1}".format(
                        inscription_produit.distribution.date_heure.year,
                        '<a href="{0}" target="_blank">Cliquez ici pour adhérer.</a>'.format(
                            reverse("register_adhesion") + "?annee=" + str(inscription_produit.distribution.date_heure.year)
                        )
                    ))

            if qt > inscription_produit.unites_dispo or qt > inscription_produit.unites_restantes:
                raise ValueError(
                    "Il n'y a pas assez d'unités disponibles pour le produit {0} à la distribution {1}.".format(
                        produit.nom, distribution.date_heure.strftime("%d/%m/%Y")
                    ))

            if produit.ferme.id not in new_contrats_list:
                new_contrats_list[produit.ferme.id] = Contrat(date_signature=None,
                                                              numero_groupe=group_number,
                                                              ferme=produit.ferme,
                                                              souscripteur=utilisateur)
                new_contrats_list[produit.ferme.id].save()

            contrat = new_contrats_list[produit.ferme.id]
            # inscription adhérent temporaire. Ne sera valide que lorsque le contrat aura été signé
            inscription_adherent = InscriptionAdherent(
                unites_commandees=qt,
                distribution=distribution,
                produit=produit,
                inscription_produit=inscription_produit,
                utilisateur=utilisateur,
                contrat=contrat
            )
            inscription_adherent.save()

class DistributionSpecialeView(RegisterInscriptionAdherentProduitView):
    def get(self, request, id_distribution):
        return super().get(request, distrib_unique=self.get_distrib(id_distribution))
    
    def post(self, request, id_distribution):
        return super().post(request, distrib_unique=self.get_distrib(id_distribution))
    
    def get_distrib(self, id_distribution):
        distrib = Distribution.get_or_404(id_distribution)
        if distrib.date_visibilite_utilisateurs and distrib.date_visibilite_utilisateurs > datetime.now():
            raise Http404()
        return distrib

class RecapReglementsInscriptionView(View):
    template_name = "distribs/inscription/recap_reglement_inscription.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.is_legitimate, login_url="email_not_confirmed_page", redirect_field_name=None
    ))
    def get(self, request, id_groupe):
        contrats = Contrat.objects.filter(
            numero_groupe=id_groupe,
            souscripteur_id=request.user.id,
            date_signature__isnull=True
        )
        if len(contrats) == 0:
            return HttpResponseNotFound("La procédure d'inscription demandée n'existe pas ou plus")

        return render(request, self.template_name, {
            "contrats": contrats,
            "id_groupe": id_groupe
        })


class FinalisationInscriptionView(View):
    template_name = "distribs/inscription/finalisation_inscription.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.is_legitimate, login_url="email_not_confirmed_page", redirect_field_name=None
    ))
    def get(self, request, id_groupe):
        contrats = Contrat.objects.filter(
            numero_groupe=id_groupe,
            souscripteur_id=request.user.id,
            date_signature__isnull=True
        )
        if len(contrats) == 0:
            return HttpResponseNotFound("La procédure d'inscription demandée n'existe pas ou plus")

        form = FinalisationInscriptionDistributionsForm(contrats)

        return render(request, self.template_name, {
            "contrats": contrats,
            "id_groupe": id_groupe,
            "form": form
        })

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.is_legitimate, login_url="email_not_confirmed_page", redirect_field_name=None
    ))
    def post(self, request, id_groupe):
        contrats = Contrat.objects.filter(
            numero_groupe=id_groupe,
            souscripteur_id=request.user.id,
            date_signature__isnull=True
        )
        if len(contrats) == 0:
            return HttpResponseNotFound("La procédure d'inscription demandée n'existe pas ou plus")

        '''
        vérifier que les cases sont cochées
        
        de manière atomique: 
          vérifier qu'il reste assez d'unités de tout 
            -> sinon, afficher une erreur [amène sur le chemin alternatif pour éditer ses choix, TODO ] 
          mettre à jour les quantités disponibles pour chaque inscription
        
        signer les contrats
        
        rediriger vers une page de confirmation
        
        '''
        form = FinalisationInscriptionDistributionsForm(contrats, request.POST)

        if form.is_valid():
            # check that amounts are still available, then account them
            with transaction.atomic():
                tout_est_dispo = True
                for contrat in contrats:
                    for inscription_adherent in contrat.inscriptions.all():
                        diff = inscription_adherent.unites_commandees - inscription_adherent.inscription_produit.unites_restantes
                        if diff > 0:
                            form.add_error(
                                None,
                                "Il manque {0} unités pour le produit {1}. Ne nous pourrons malheureusement pas donner suite à cette commande.".format(
                                    diff, inscription_adherent.produit.nom))
                            tout_est_dispo = False

                if not tout_est_dispo:
                    return render(request, self.template_name, {
                        "contrats": contrats,
                        "id_groupe": id_groupe,
                        "form": form
                    })

                for contrat in contrats:
                    for inscription_adherent in contrat.inscriptions.all():
                        qt = inscription_adherent.unites_commandees
                        inscription_adherent.inscription_produit.unites_restantes -= qt
                        inscription_adherent.inscription_produit.save()

            # sign contracts
            for contrat in contrats:
                contrat.date_signature = datetime.now()
                contrat.save()

                montant = contrat.valeur_totale()
                operation = OperationUtilisateur(
                    montant=montant,
                    ferme=contrat.ferme,
                    utilisateur=contrat.souscripteur,
                    date_enregistrement=datetime.now(),
                    libelle="Paiement vers la ferme {0} pour le contrat n°{1}".format(
                        contrat.ferme.nom,
                        contrat.id
                    ),
                    statut=OperationUtilisateur.Statuts.OPERATION_ENREGISTREE,
                    contrat=contrat
                )
                operation.save()

            if request.user.essai_gratuit:
                request.user.essai_gratuit = False
                request.user.save()

            return redirect(to="confirmation_inscription_distrib")

        return render(request, self.template_name, {
            "contrats": contrats,
            "id_groupe": id_groupe,
            "form": form
        })

class ConfirmationIncriptionView(View):
    template_name = "distribs/inscription/confirmation_inscription.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.is_legitimate, login_url="email_not_confirmed_page", redirect_field_name=None
    ))
    def get(self, request):
        
        return render(request, self.template_name, {"PAGE_SUCCES_CONTRAT": GaiaHypertext.find_content("PAGE_SUCCES_CONTRAT")})

class ContratPDFView(View):
    template_name = "distribs/contrat/contrat_pdf.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.is_legitimate, login_url="email_not_confirmed_page", redirect_field_name=None
    ))
    def get(self, request, id_contrat):
        try:
            contrat = Contrat.objects.filter(souscripteur=request.user).get(id=id_contrat)
        except Contrat.DoesNotExist:
            return HttpResponseNotFound("Le contrat spécifié n'existe pas !")

        produits_disponibles = {}
        inscriptions_produits = {}
        for distrib in contrat.distributions():
            for inscription_produit in distrib.inscriptions_produits.filter(produit__ferme=contrat.ferme):
                prod = inscription_produit.produit
                if prod.id not in produits_disponibles:
                    produits_disponibles[prod.id] = prod
                    inscriptions = prod.inscriptions_between(
                        contrat.date_debut(), contrat.date_fin()
                    ).order_by("distribution__date_heure")

                    inscriptions_produits[prod.id] = []
                    for ins in inscriptions:
                        inscriptions_produits[prod.id].append({
                            "date": ins.distribution.date_heure.date(),
                            "unites_dispo": ins.unites_dispo
                        })

        produits_commandes = contrat.produits_concernes()
        inscriptions_adherent = {}
        for produit in produits_commandes:
            inscriptions_adherent[produit.id] = []
            inscriptions = contrat.inscriptions.filter(
                produit=produit
            ).order_by("distribution__date_heure")
            for ins in inscriptions:
                inscriptions_adherent[produit.id].append({
                    "date": ins.distribution.date_heure.date(),
                    "unites_commandees": ins.unites_commandees
                })

        template = get_template(self.template_name)
        html = template.render({
            "contrat": contrat,
            "date_debut_contrat": contrat.date_debut(),
            "date_fin_contrat": contrat.date_fin(),
            "nb_livraisons_contrat": contrat.nombre_livraisons(),
            "souscripteur": contrat.souscripteur,
            "ferme": contrat.ferme,
            "produits_disponibles": list(produits_disponibles.values()),
            "inscriptions": inscriptions_produits,
            "produits_commandes": produits_commandes,
            "inscriptions_adherent": inscriptions_adherent,
            "tarif_adhesion": str(GaiaSetting.get_value("TARIF_ADHESION")) + "€",
        })

        result = BytesIO()
        pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), result)
        return HttpResponse(result.getvalue(), content_type="application/pdf")


class ContratDetailsView(View):
    template_name = "distribs/contrat/contrat_details.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.is_gestionnaire, login_url="home_page", redirect_field_name=None
    ))  # TODO temporary lock, this view is admin only for now
    @method_decorator(user_passes_test(
        Utilisateur.is_legitimate, login_url="email_not_confirmed_page", redirect_field_name=None
    ))
    def get(self, request, id_contrat):
        contrat = Contrat.get_or_404(id_contrat)
        inscriptions = InscriptionAdherent.objects.filter(
            contrat_id=contrat.id
        ).order_by("distribution__date_heure")

        return render(request, self.template_name, {
            "contrat": contrat,
            "inscriptions": inscriptions
        })


class RemoveContratView(View):
    template_name = "distribs/contrat/remove_contrat.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.is_gestionnaire, login_url="home_page", redirect_field_name=None
    ))  # TODO temporary lock, this view is admin only for now
    @method_decorator(user_passes_test(
        Utilisateur.is_legitimate, login_url="email_not_confirmed_page", redirect_field_name=None
    ))
    def get(self, request, id_contrat):
        contrat = Contrat.get_or_404(id_contrat)

        return render(request, self.template_name, {
            "contrat": contrat
        })

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.is_gestionnaire, login_url="home_page", redirect_field_name=None
    ))  # TODO temporary lock, this view is admin only for now
    @method_decorator(user_passes_test(
        Utilisateur.is_legitimate, login_url="email_not_confirmed_page", redirect_field_name=None
    ))
    def post(self, request, id_contrat):
        contrat = Contrat.get_or_404(id_contrat)
        inscriptions = InscriptionAdherent.objects.filter(
            contrat_id=contrat.id
        ).order_by("distribution__date_heure")

        for inscription in inscriptions:
            if inscription.contrat.date_signature is not None:
                inscription.inscription_produit.unites_restantes += inscription.unites_commandees
                inscription.inscription_produit.save()

        inscriptions.delete()
        contrat.delete()

        return redirect(to="search_contrats")


class SearchContratsView(ListView):
    model = Contrat
    template_name = "distribs/contrat/contrats_list.html"
    context_object_name = "contrats"
    paginate_by=25

    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_gestionnaire, login_url="profile_home", redirect_field_name=None))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_queryset(self):
        user_filter = self.request.GET.get("user_id")
        include_archive = "archive" in self.request.GET
        queryset = Contrat.objects.filter(date_signature__isnull=False)
        if not include_archive:
            queryset = queryset.filter(archive=False)

        if user_filter:
            try:
                user_filter = int(user_filter)
                Utilisateur.get_or_404(user_filter)
                queryset = queryset.filter(souscripteur__id=user_filter)
            except (ValueError, TypeError):
                raise Http404()
        
        return queryset.order_by("-date_signature")


class RemoveInscriptionContratView(View):
    template_name = "distribs/contrat/remove_inscription_contrat.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.is_gestionnaire, login_url="home_page", redirect_field_name=None
    ))  # TODO temporary lock, this view is admin only for now
    @method_decorator(user_passes_test(
        Utilisateur.is_legitimate, login_url="email_not_confirmed_page", redirect_field_name=None
    ))
    def get(self, request, id_inscription):
        inscription = InscriptionAdherent.get_or_404(id_inscription)

        return render(request, self.template_name, {
            "inscription": inscription
        })

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.is_gestionnaire, login_url="home_page", redirect_field_name=None
    ))  # TODO temporary lock, this view is admin only for now
    @method_decorator(user_passes_test(
        Utilisateur.is_legitimate, login_url="email_not_confirmed_page", redirect_field_name=None
    ))
    def post(self, request, id_inscription):
        inscription = InscriptionAdherent.get_or_404(id_inscription)
        contrat_id = inscription.contrat.id

        if inscription.contrat.date_signature is not None:
            inscription.inscription_produit.unites_restantes += inscription.unites_commandees
            inscription.inscription_produit.save()

        inscription.contrat.date_modification = datetime.today()
        inscription.contrat.save()
        inscription.delete()

        return redirect(to="contrat_details", id_contrat=contrat_id)

class EmargementView(View):
    template_name = "distribs/emargement.html"
    
    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_gestionnaire, login_url="profile_home", redirect_field_name=None))
    def get(self, request, id_distribution):
        distrib = Distribution.get_or_404(id_distribution)
        letter = request.GET.get('page', 'A')

        form = EmargementForm(id_distribution, letter)
        
        return render(request, self.template_name, {
            "form": form,
            "distribution": distrib
        })
    
    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_gestionnaire, login_url="profile_home", redirect_field_name=None))
    def post(self, request, id_distribution):
        distrib = Distribution.get_or_404(id_distribution)
        letter = request.POST.get('page', 'A')
        form = EmargementForm(id_distribution, letter, request.POST)
        if form.is_valid():
            for field in form.cleaned_data:
                if form.cleaned_data[field] is None:
                    continue
                try:
                    inscription_id = int(field.split("inscription_")[1])
                    inscription = InscriptionAdherent.get_or_404(inscription_id)
                    inscription.recupere = form.cleaned_data[field]
                    inscription.save()
                except InscriptionAdherent.DoesNotExist:
                    form.add_error(None, "Une des inscriptions n'existe pas.")
        
        return render(request, self.template_name, {
            "form": form,
            "distribution": distrib
        })

class QuickActionView(View):

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.is_gestionnaire, login_url="home_page", redirect_field_name=None
    ))  # TODO this view should be removed as soon as the database is stabilized
    def get(self, request):
        action = request.GET.get("action")
        if action is None:
            raise Http404()

        if action == "recompute_unites_restantes":
            return self.recompute_unites_restantes(request)
        elif action == "remove_adhesions_year":
            return self.remove_adhesions_year(request)
        elif action == "recompute_unpaid_operations_utilisateurs":
            return self.recompute_unpaid_operations_utilisateurs(request)
        elif action == "remove_abandonned_contrats":
            Contrat.supprimer_contrats_abandonnes()
            return HttpResponse("OK")
        else:
            raise Http404()

    def recompute_unites_restantes(self, request):
        inscriptions_produits = InscriptionProduit.objects.all()
        for inscription in inscriptions_produits:
            total_amount = 0
            for inscription_adherent in inscription.inscriptions_adherents.all():
                total_amount += inscription_adherent.unites_commandees
            inscription.unites_restantes = inscription.unites_dispo - total_amount
            inscription.save()

        return HttpResponse("Unités restantes mises à jour !")

    def remove_adhesions_year(self, request):
        year = request.GET.get("year")
        if year is None:
            return HttpResponse("Il manque un argument GET year pour l'année")
        else:
            try:
                year = int(year)
                adhesions = Adhesion.objects.filter(annee=year, payee=False)
                adhesions.delete()
            except:
                raise Http404()

        return HttpResponse("Opération effectuée !")

    def recompute_unpaid_operations_utilisateurs(self, request):
        # TODO SHOULD ONLY BE USED ONCE. NOT SUITED FOR REGULAR USE.

        OperationUtilisateur.objects.filter(
            statut=OperationUtilisateur.Statuts.OPERATION_ENREGISTREE
        ).delete()

        contrats = Contrat.objects.filter(date_signature__isnull=False)
        adhesions = Adhesion.objects.all()

        for contrat in contrats:
            montant = contrat.valeur_totale()
            operation = OperationUtilisateur(
                montant=montant,
                ferme=contrat.ferme,
                utilisateur=contrat.souscripteur,
                date_enregistrement=contrat.date_signature,
                libelle="Paiement vers la ferme {0} pour le contrat n°{1}".format(
                    contrat.ferme.nom,
                    contrat.id
                ),
                statut=OperationUtilisateur.Statuts.OPERATION_ENREGISTREE,
                contrat=contrat
            )
            operation.save()

        for adhesion in adhesions:
            operation = OperationUtilisateur(
                montant=adhesion.tarif,
                ferme=None,
                contrat=None,
                utilisateur=adhesion.utilisateur,
                date_enregistrement=adhesion.date_enregistrement,
                libelle="Adhésion du {0} pour l'année {1}".format(
                    adhesion.date_enregistrement.strftime("%d/%m/%Y"), adhesion.annee
                ),
                statut=OperationUtilisateur.Statuts.OPERATION_ENREGISTREE,
            )
            operation.save()

        return HttpResponse("Opérations réinitialisées !")

