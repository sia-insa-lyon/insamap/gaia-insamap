from datetime import timedelta, datetime, date

from dateutil.relativedelta import relativedelta
from django.db import models
from django.db.models import Max, Min, Q, Count
from django.forms import ValidationError
from django.http import Http404
from django.contrib import admin

from Gaia import settings
from gaia_paysans.models import Produit
from gaia_utils.models import GaiaSetting

class Distribution(models.Model):
    date_heure = models.DateTimeField(null=False, unique=True, verbose_name="Date et Heure")
    duree = models.TimeField(null=False, verbose_name="Durée")
    lieu = models.CharField(blank=True, max_length=128)
    date_visibilite_paysans = models.DateTimeField(null=True, blank=True)
    date_visibilite_utilisateurs = models.DateTimeField(null=True, blank=True)
    archive = models.BooleanField(default=False)

    def __str__(self):
        return "Livraison du {0} à {1} (durée: {2})".format(
            self.date_heure.strftime("%d/%m/%Y"),
            self.date_heure.strftime("%H:%M"),
            self.duree.strftime("%H:%M")
        )
    
    def clean_fields(self, exclude=None):
        super().clean_fields(exclude=exclude)
        
        if self.archive == True and self.date_heure > datetime.now():
            raise ValidationError("Une distribution ne peut être archivée que si elle est passée")

    def date_heure_fin(self):
        return self.date_heure + relativedelta(hours=self.duree.hour, minutes=self.duree.minute,
                                               seconds=self.duree.second)

    @staticmethod
    def get_or_404(distribution_id):
        try:
            distribution = Distribution.objects.get(id=distribution_id)
            return distribution
        except Distribution.DoesNotExist:
            raise Http404()
    
    @property
    @admin.display(description="Nombre d'inscriptions produit")
    def get_nb_inscriptions_produit(self):
        return self.inscriptions_produits.count()
    
    def is_a_special_distribution(self):
        ins = self.inscriptions_produits.filter(livraison_unique=True)
        ins = [x for x in ins if Produit.Frequences.UNIQUE in x.produit.frequences]
        return len(ins) > 0

    @staticmethod
    def archivage_auto():
        delai = GaiaSetting.get_value("DELAI_ARCHIVAGE_DISTRIBUTIONS")
        if delai > 0:
            Distribution.objects.filter(archive=False, date_heure__lt=(datetime.now() - timedelta(days=delai))).update(archive=True)
    
class LastCreatedDistributions(models.Model):
    distribution = models.ForeignKey(Distribution, on_delete=models.CASCADE)


class InscriptionProduit(models.Model):
    produit = models.ForeignKey("gaia_paysans.Produit", on_delete=models.PROTECT, related_name="inscriptions_produits")
    distribution = models.ForeignKey(Distribution, on_delete=models.CASCADE, related_name="inscriptions_produits")

    tarif_vente = models.FloatField(null=True, blank=True, verbose_name="Tarif unitaire",
                                    help_text="Si vide, prend les informations de la fiche produit")
    quantite_unitaire = models.CharField(null=True, blank=True, max_length=20, verbose_name="Quantité unitaire",
                                         help_text="Si vide, prend les informations de la fiche produit")

    unites_dispo = models.PositiveIntegerField(null=False)
    unites_restantes = models.PositiveIntegerField(null=True, blank=True,
        help_text="Les unités restantes pour cette inscription. Il est préférable de ne pas l'ajuster manuellement.")
    
    delai_max_fermeture_inscriptions = models.PositiveIntegerField(null=False, help_text="En jours complets entre la date limite de fermeture des inscriptions et la date de la distribution.<br/>\nExemple : Pour une distribution un lundi et une fermeture le jeudi précédent à 23h59, mettre 3 jours (Vendredi, Samedi, Dimanche)")

    signature_amap = models.BooleanField(default=False)
    signature_ferme = models.BooleanField(default=False)

    livraison_unique = models.BooleanField(default=False, verbose_name="Autoriser en livraison unique ?", help_text="Si coché, les produits disponibles en livraison unique seront proposés à la vente pour cette distribution.<br/>\nSi désactivé, le produit n'est pas proposé en livraison unique")
    adhesion_requise = models.BooleanField(default=False, verbose_name="Adhésion requise pour livraison unique ?", help_text="Si désactivé, le produit n'est pas proposé en livraison unique")
    
    class Meta:
        unique_together = (("produit", "distribution"),)

    def save(self, *args, **kwargs):
        if self.tarif_vente is None:
            self.tarif_vente = self.produit.tarif_reference
        if self.quantite_unitaire is None:
            self.quantite_unitaire = self.produit.quantite_reference
        if self.unites_restantes is None:
            self.unites_restantes = self.unites_dispo

        super().save(*args, **kwargs)

        for inscription_adherent in self.inscriptions_adherents.all():
            inscription_adherent.save()  # will update distribution & produit fields

    def date_cloture_inscriptions(self):
        return self.distribution.date_heure - timedelta(days=self.delai_max_fermeture_inscriptions)

    def __str__(self):
        return "Inscription produit '{0}' le {1} à {2} pour {3}€".format(
            self.produit.nom,
            self.distribution.date_heure.strftime("%d/%m/%Y"),
            self.distribution.date_heure.strftime("%H:%M"),
            self.tarif_vente
        )

    @staticmethod
    def get_or_404(inscription_id):
        try:
            inscription = InscriptionProduit.objects.get(id=inscription_id)
            return inscription
        except InscriptionProduit.DoesNotExist:
            raise Http404()
    
    @admin.display(description="Signatures", boolean=True)
    def _all_signatures(self):
        return self.signature_amap and self.signature_ferme

class Contrat(models.Model):
    date_creation = models.DateField(null=True)
    date_signature = models.DateField(null=True, blank=True)  # lorsque les inscriptions sont validées
    date_modification = models.DateField(null=True)
    numero_groupe = models.IntegerField(
        verbose_name="Numéro identique pour tous les contrats souscrits au même moment par le même utilisateur"
    )

    souscripteur = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, related_name="contrats",
                                     null=False)
    ferme = models.ForeignKey("gaia_paysans.Ferme", on_delete=models.PROTECT, related_name="contrats",
                              null=False)
    
    archive = models.BooleanField(default=False)

    def clean_fields(self, exclude=None):
        super().clean_fields(exclude=exclude)

        if not self.numero_groupe:
            self.numero_groupe = Contrat.next_numero_groupe(self.souscripteur.id)

        if self.date_signature and self.date_signature < (self.date_creation if self.date_creation else datetime.now().date()):
            raise ValidationError("La date de signature ne peut pas être antérieure à la date de création")

        if self.archive == True:
            distributions_id = self.inscriptions.values_list('distribution', flat=True).distinct()
            distributions = Distribution.objects.filter(id__in=distributions_id, archive=False)
            if distributions.exists():
                raise ValidationError("Impossible d'archiver un contrat contenant des distributions non archivées")
         
    def save(self, *args, **kwargs):
        if not self.id:  # uniquement lors de la première sauvegarde du Contrat
            self.date_creation = datetime.now()

        if self.date_signature and not self.date_modification:
            self.date_modification = datetime.now()

        super(Contrat, self).save(*args, **kwargs)

    @staticmethod
    def next_numero_groupe(id_utilisateur):
        numero_max = Contrat.objects.filter(
            souscripteur_id=id_utilisateur
        ).aggregate(
            Max("numero_groupe")
        )["numero_groupe__max"]

        if numero_max is None:
            return 0
        else:
            return numero_max + 1

    def date_debut(self):
        if self.inscriptions.count() > 0:
            return self.inscriptions.aggregate(
                Min("distribution__date_heure")
            )["distribution__date_heure__min"]
        else:
            return datetime.combine(self.date_signature, datetime.min.time())

    def date_fin(self):
        if self.inscriptions.count() > 0:
            return self.inscriptions.aggregate(
                Max("distribution__date_heure")
            )["distribution__date_heure__max"]
        else:
            return datetime.combine(self.date_signature, datetime.min.time())


    def nombre_livraisons(self):
        nb_livraisons = self.inscriptions.distinct("distribution").count()
        return nb_livraisons

    def produits_concernes(self):
        produits = [inscription.produit for inscription in self.inscriptions.distinct("produit")]
        return produits

    def distributions(self):
        distributions = [inscription.distribution for inscription in self.inscriptions.distinct("distribution")]
        return distributions

    def valeur_totale(self):
        valeur = 0.0
        for inscription in self.inscriptions.all():
            valeur += inscription.unites_commandees * inscription.inscription_produit.tarif_vente
        return valeur

    def semestre_contrat(self):
        if 2 <= self.date_signature.month <= 5:
            return "S2"
        elif 6 <= self.date_signature.month <= 9:
            return "summer"
        elif 9 <= self.date_signature.month or self.date_signature.month <= 1:
            return "S1"

    def __str__(self):
        if self.date_signature is not None:
            return "Contrat n°{0} ({1} - {2}) du {3} pour {4} distribution(s)".format(
                self.id, self.ferme.nom, self.souscripteur.email,
                self.date_signature.strftime("%d/%m/%Y"), self.nombre_livraisons()
            )
        else:
            return "Contrat en cours de création ({0} - {1})".format(
                self.ferme.nom, self.souscripteur.email
            )
    
    def statut(self):
        if self.date_signature is not None:
            return "Signé le " + self.date_signature.strftime("%d/%m/%Y")
        else:
            return "En cours de création"
        
    @staticmethod
    def get_or_404(contrat_id):
        try:
            contrat = Contrat.objects.get(id=contrat_id)
            return contrat
        except Contrat.DoesNotExist:
            raise Http404()

    @staticmethod
    def supprimer_contrats_abandonnes():
        Contrat.objects.filter(date_signature__isnull=True).filter(
            Q(date_creation__isnull=True) | Q(date_creation__lt=datetime.now() - timedelta(days=1))
        ).delete()

    @staticmethod
    def archivage_auto():
        delai = GaiaSetting.get_value("DELAI_ARCHIVAGE_CONTRATS")
        if delai > 0:
            date_limite_archivage = datetime.now() - timedelta(days=delai)
            contrats = Contrat.objects.filter(archive=False, date_signature__isnull=False).annotate(
            date_fin=Max("inscriptions__distribution__date_heure"), 
            non_archive=Count("inscriptions__distribution__id", filter=Q(inscriptions__distribution__archive=False), distinct=True),
            ).filter(date_fin__lt=date_limite_archivage, non_archive=0)
            try:
                contrats.update(archive=True)
            except ValidationError:
                pass

class InscriptionAdherent(models.Model):
    unites_commandees = models.PositiveIntegerField(null=False)

    distribution = models.ForeignKey(Distribution, on_delete=models.PROTECT, related_name="inscriptions_adherents")
    produit = models.ForeignKey("gaia_paysans.Produit", on_delete=models.PROTECT, related_name="inscriptions_adherents")
    inscription_produit = models.ForeignKey(InscriptionProduit, on_delete=models.PROTECT,
                                            related_name="inscriptions_adherents")
    utilisateur = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=False,
                                    related_name="inscriptions_distributions")
    contrat = models.ForeignKey(Contrat, on_delete=models.CASCADE, related_name="inscriptions")

    recupere = models.BooleanField(default=False, verbose_name="Récupéré")

    def __str__(self):
        return "Inscription distribution du {0} par {1} pour {2} x {3} de {4} (contrat n°{5})".format(
            self.distribution.date_heure.strftime("%d/%m/%Y"), self.utilisateur.email,
            self.unites_commandees, self.inscription_produit.quantite_unitaire, self.produit.nom, self.contrat_id
        )
    
    def clean_fields(self, exclude=None):
        super().clean_fields(exclude=exclude)
        if self.inscription_produit.produit.ferme != self.contrat.ferme:
            raise ValidationError("Le produit n'appartient pas à la ferme du contrat")

    def save(self, *args, **kwargs):
        if not self.utilisateur:
            self.utilisateur = self.contrat.souscripteur
        self.distribution = self.inscription_produit.distribution
        self.produit = self.inscription_produit.produit

        if self.contrat.date_signature:
            self.contrat.date_modification = datetime.now()
            self.contrat.save()

        super().save(*args, **kwargs)

    @staticmethod
    def get_or_404(inscription_id):
        try:
            inscription = InscriptionAdherent.objects.get(id=inscription_id)
            return inscription
        except InscriptionAdherent.DoesNotExist:
            raise Http404()
