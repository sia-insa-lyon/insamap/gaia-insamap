import enum
from collections import OrderedDict

from django import forms
from django.core.exceptions import ValidationError
from django.forms import MultiValueField, IntegerField, MultiWidget, NumberInput
from django.http import Http404
from django.urls import reverse

from datetime import datetime
from Gaia import settings
from gaia_utils.widgets import GaiaDateInput, GaiaTimeInput, GaiaDateTimeInput
from gaia_utils.models import GaiaSetting
from gaia_distribs.models import InscriptionAdherent


class TimeUnitChoices(enum.Enum):
    DAY = 1
    WEEK = 2
    MONTH = 3
    YEAR = 4


class FastDistributionCreationForm(forms.Form):
    date_from = forms.DateField(required=True, label="Date de départ",
                                widget=GaiaDateInput())
    date_to = forms.DateField(required=True, label="Date maximale (incluse)",
                              widget=GaiaDateInput())

    time_delta = forms.IntegerField(required=True, label="Intervalle entre deux distributions", min_value=1,
                                    widget=forms.NumberInput(attrs={"class": "form-control"}))
    delta_unit = forms.ChoiceField(required=True, label="Unité", choices=[
        (TimeUnitChoices.DAY.value, "Jour(s)"),
        (TimeUnitChoices.WEEK.value, "Semaine(s)"),
        (TimeUnitChoices.MONTH.value, "Mois"),
        (TimeUnitChoices.YEAR.value, "Année(s)")
    ], initial=TimeUnitChoices.WEEK.value, widget=forms.Select(attrs={"class": "form-control"}))

    heure_debut = forms.TimeField(required=True, label="Heure de début des distributions",
                                  widget=GaiaTimeInput())
    heure_fin = forms.TimeField(required=True, label="Heure de fin des distributions",
                                widget=GaiaTimeInput())
    lieu = forms.CharField(required=False, empty_value="", max_length=128, label="Lieu des distributions (optionnel)",
                           widget=forms.TextInput(attrs={"class": "form-control"}))

    date_visibilite_paysans = forms.DateTimeField(
        required=False,
        label="Date d'apparition pour les paysans (maintenant si vide)",
        widget=GaiaDateTimeInput()
    )
    date_visibilite_utilisateurs = forms.DateTimeField(
        required=False,
        label="Date d'apparition pour les utilisateurs (maintenant si vide)",
        widget=GaiaDateTimeInput()
    )

    def clean(self):
        cleaned_data = super().clean()
        date_from = cleaned_data.get("date_from")
        date_to = cleaned_data.get("date_to")

        if date_from > date_to:
            raise ValidationError(
                "La date de début doit être inférieure ou égale à celle de fin."
            )

class ProductsRegistrationsForm(forms.Form):

    signature_amap = forms.BooleanField(
        required=False,
        initial=True,
        label="Ajouter la signature de l'AMAP à toutes les inscriptions ?",
        widget=forms.CheckboxInput(attrs={"class": "form-check-input"})
    )

    signature_ferme = forms.BooleanField(
        required=False,
        initial=False,
        label="Ajouter la signature de la ferme à toutes les inscriptions ?",
        widget=forms.CheckboxInput(attrs={"class": "form-check-input"})
    )

    def __init__(self, distributions, produits, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.distributions = distributions
        self.produits = produits
        self.produits_par_ferme = dict()

        for distrib in distributions:
            self.fields["distribution_" + str(distrib.id)] = forms.BooleanField(
                required=False,
                initial=False,
                label="{0}".format(
                    distrib.date_heure.strftime("%d/%m/%Y à %H:%M")
                ),
                widget=forms.CheckboxInput(attrs={"class": "form-check-input"})
            )
        
        for produit in produits:
            if produit.ferme not in list(self.produits_par_ferme.keys()):
                self.produits_par_ferme[produit.ferme] = []
            self.produits_par_ferme[produit.ferme].append(produit)
            self.fields["produit_" + str(produit.id)] = forms.BooleanField(
                required=False,
                initial=False,
                label=produit.nom,
                widget=forms.CheckboxInput(attrs={"class": "form-check-input"})
            )
            self.fields["produit_" + str(produit.id) + "_unites_dispo"] = forms.IntegerField(
                required=False,
                initial=0,
                min_value=0,
                label="Quantité disponible",
                widget=forms.NumberInput(attrs={"class": "form-control", "min":"0", "style":"width: 5em; direction: rtl"})
            )
            self.fields["produit_" + str(produit.id) + "_delai_max"] = forms.IntegerField(
                required=False,
                initial=0,
                min_value=0,
                label="Délai maximal de fermeture des commande (en jours)",
                widget=forms.NumberInput(attrs={"class": "form-control", "min":"0", "style":"width: 5em; direction: rtl"})
            )

class ProduitAmountWidget(NumberInput):
    template_name = "distribs/widgets/produit_amount_widget.html"

    def __init__(self, inscription_produit):
        super().__init__(attrs={
            "class": "form-control",
            "min": 0,
            "max": min(GaiaSetting.get_value("QUANTITE_PRODUIT_MAX_UTILISATEUR"), inscription_produit.unites_restantes)
        })
        self.inscription_produit = inscription_produit
        self.quantite_dispo = inscription_produit.unites_restantes
        self.tarif_vente = inscription_produit.tarif_vente

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        context['widget']['label'] = self.inscription_produit.produit.nom
        context['widget']['produit'] = {
            "quantite": self.inscription_produit.quantite_unitaire,
            "tarif_reference": self.inscription_produit.produit.tarif_reference,
            "tarif_vente": self.tarif_vente,
            "ferme": self.inscription_produit.produit.ferme.nom,
            "description": self.inscription_produit.produit.description,
            "commande_max": min(GaiaSetting.get_value("QUANTITE_PRODUIT_MAX_UTILISATEUR"), self.quantite_dispo)
        }
        return context


class DistributionProduitChoiceWidget(MultiWidget):

    def __init__(self, produits, *args, **kwargs):
        widgets = OrderedDict()
        for inscription in produits:
            widgets["produit_" + str(inscription.produit_id)] = ProduitAmountWidget(inscription_produit=inscription)

        super().__init__(widgets=widgets)

    def decompress(self, value):
        if value:
            values = dict()
            tuples = value.split(';')
            for entry in tuples:
                produit_id, produit_amount = entry.split("=")
                values["_produit_" + str(produit_id)] = produit_amount

            result = []
            for key in self.widgets_names:
                if key in values:
                    result.append(values[key])
                else:
                    result.append(None)

            return result
        return []

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        return context


class ProduitAmountField(IntegerField):

    def __init__(self, inscription_produit, **kwargs):
        super().__init__(
            min_value=0,
            max_value=GaiaSetting.get_value("QUANTITE_PRODUIT_MAX_UTILISATEUR"), 
            # Le cas où l'utilisateur veut commander plus que ce qui est disponible est géré lors de la soumission du formulaire
            label=inscription_produit.produit.nom,
            **kwargs
        )
        self.produit = inscription_produit.produit

    def clean(self, value):
        cleaned_value = super().clean(value)
        if cleaned_value is None:
            cleaned_value = 0
        return str(self.produit.id) + "=" + str(cleaned_value)


class DistributionProduitChoiceField(MultiValueField):

    def __init__(self, distribution, produits, *args, **kwargs):
        widget = DistributionProduitChoiceWidget(produits)

        fields = [
            ProduitAmountField(
                inscription,
                localize=False,
                required=False,
            )
            for inscription in produits
        ]

        super().__init__(fields=fields, widget=widget, require_all_fields=False, **kwargs)

        self.distribution = distribution
        self.nb_produits = len(fields)

        self.label = "Le {0} de {1} à {2}".format(
            self.distribution.date_heure.strftime("%d/%m/%Y"),
            self.distribution.date_heure.strftime("%H:%M"),
            self.distribution.date_heure_fin().strftime("%H:%M")
        )

        self.label_suffix = ""

    def compress(self, data_list):
        if data_list:
            res = ""
            for element in data_list:
                res += str(element) + ";"
            return res[:-1]
        return None


class RegisterAdherentDistribsForm(forms.Form):
    # dates_dispo = liste des dates possibles

    # produits choisis par date :
    # pour chaque date dispo, une liste de (id_produit, quantite)

    def __init__(self, distributions, produits_par_distrib, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.distributions = distributions
        for distrib in distributions:
            self.fields[
                "distribution_" + str(distrib.id)
                ] = DistributionProduitChoiceField(distrib, produits_par_distrib[distrib.id], required=False)


class FinalisationInscriptionDistributionsForm(forms.Form):
    charte_amap = forms.BooleanField(required=True,
                                     initial=False)

    def __init__(self, contrats, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.contrats = contrats
        for contrat in self.contrats:
            route_for_contrat = reverse("contrat_pdf", kwargs={"id_contrat": contrat.id})
            self.fields[
                "check_contrat_" + str(contrat.id)
            ] = forms.BooleanField(
                required=True,
                label="J'accepte les termes du <a target='blank' href='" + route_for_contrat +
                      "'>contrat juridique avec " + contrat.ferme.nom + "</a>",
                initial=False,
                widget=forms.CheckboxInput(attrs={
                    "contrat": contrat.id
                })
            )

class EmargementForm(forms.Form):


    def __init__(self, distribution_id, letter="A", *args, **kwargs):
        super().__init__(*args, **kwargs)
        letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

        try:
            letter = letters.index(letter)
            letter = letters[letter]
        except ValueError:
             raise Http404()

        inscriptions_adherents = InscriptionAdherent.objects.filter(
            distribution_id=distribution_id
        ).filter(contrat__date_signature__isnull=False).filter(utilisateur__last_name__istartswith=letter).order_by("utilisateur__last_name", "utilisateur__first_name", "produit__ferme__nom", "produit__nom")

        self.inscriptions = {}

        for inscription in inscriptions_adherents:
            if inscription.utilisateur not in self.inscriptions:
               self.inscriptions[inscription.utilisateur] = {}
            if inscription.produit.ferme not in self.inscriptions[inscription.utilisateur]:
                self.inscriptions[inscription.utilisateur][inscription.produit.ferme] = []
            self.inscriptions[inscription.utilisateur][inscription.produit.ferme].append(inscription)
            self.fields["inscription_" + str(inscription.id)] = forms.BooleanField(
                required=False,
                initial=inscription.recupere,
                label=f"{ inscription.produit.nom } - {inscription.unites_commandees} unités ({round(inscription.unites_commandees*inscription.inscription_produit.tarif_vente, 2)} €)",
                widget=forms.CheckboxInput(attrs={"class": "form-check-input inscriptions_check me-1", "farm_id":inscription.produit.ferme.id, "user_id":inscription.utilisateur.id})
            )
