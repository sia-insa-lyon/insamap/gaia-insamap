from django.contrib.auth.decorators import login_required, user_passes_test
from django.urls import path
from django.views.generic import RedirectView, TemplateView

from gaia_distribs.views import RegisterMultipleDistributionsView, MyContratsView, \
    RegisterInscriptionAdherentProduitView, RecapReglementsInscriptionView, ContratPDFView, FinalisationInscriptionView, ConfirmationIncriptionView, \
    ContratDetailsView, RemoveContratView, SearchContratsView, EmargementView, RemoveInscriptionContratView, QuickActionView, ProductsRegistrationsAdminView, DistributionSpecialeView
from gaia_users.models import Utilisateur

urlpatterns = [
    path('multiple-create', RegisterMultipleDistributionsView.as_view(), name="register_multiple_distributions"),
    path('contrats', MyContratsView.as_view(), name="my_contrats"),

    path('products-registrations', ProductsRegistrationsAdminView.as_view(), name="products_registrations"),

    path('inscription', RedirectView.as_view(pattern_name="register_adherent_distributions", permanent=True),
         name="register_adherent_distributions_public_alias"),
    path('inscription/choix', RegisterInscriptionAdherentProduitView.as_view(),
         name="register_adherent_distributions"),
    path('inscription/reglement/<int:id_groupe>', RecapReglementsInscriptionView.as_view(),
         name="recap_reglements_inscription"),
    path('inscription/finalisation/<int:id_groupe>', FinalisationInscriptionView.as_view(),
         name="finalisation_inscription"),
    path('inscription/confirmation', ConfirmationIncriptionView.as_view(),
         name="confirmation_inscription_distrib"),

    path('speciale/<int:id_distribution>', DistributionSpecialeView.as_view(), name="distribution_speciale"),
    path('emargement/<int:id_distribution>', EmargementView.as_view(), name="emargement"),

    path('contrat/<int:id_contrat>/pdf', ContratPDFView.as_view(), name="contrat_pdf"),

    path('contrat/<int:id_contrat>/details', ContratDetailsView.as_view(), name="contrat_details"),
    path('contrat/<int:id_contrat>/remove', RemoveContratView.as_view(), name="contrat_remove"),
    path('contrats/search', SearchContratsView.as_view(), name="search_contrats"),
    path('inscription/<int:id_inscription>/remove', RemoveInscriptionContratView.as_view(),
         name="inscription_contrat_remove"),

    # path('quick_action', QuickActionView.as_view(), name="quick_action"),
]
