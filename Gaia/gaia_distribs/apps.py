from apscheduler.schedulers.background import BackgroundScheduler
from django.apps import AppConfig


class GaiaDistribsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gaia_distribs'
    verbose_name = "Gaia - Livraisons"

    def ready(self):
        import gaia_distribs.signals
        scheduler = BackgroundScheduler()
        from gaia_distribs.models import Contrat,Distribution
        scheduler.add_job(Contrat.supprimer_contrats_abandonnes, trigger='cron', minute='0', hour='4')
        scheduler.add_job(Distribution.archivage_auto, trigger='cron', minute='5', hour='4')
        scheduler.add_job(Contrat.archivage_auto, trigger='cron', minute='10', hour='4')
        scheduler.start()

