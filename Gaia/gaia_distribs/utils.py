import datetime

from git import Repo, GitError


def get_date_interval_from_semester_name(semester_name: str, current_year: int):
    """
    Returns the date interval corresponding to the provided
    :param semester_name:
    :param current_year:
    :return:
    """
    if semester_name == "S2":
        return [
            datetime.date(year=current_year, month=2, day=1),
            datetime.date(year=current_year, month=6, day=1)
        ]
    elif semester_name == "summer":
        return [
            datetime.date(year=current_year, month=6, day=1),
            datetime.date(year=current_year, month=10, day=1)
        ]
    elif semester_name == "S1":
        return [
            datetime.date(year=current_year, month=10, day=1),
            datetime.date(year=current_year + 1, month=2, day=1)
        ]
    else:
        raise ValueError("Invalid semester name")


def get_semester_name_from_date(date: datetime.date):
    month = date.month
    if 2 <= month <= 5:
        return "S2"
    elif 6 <= month <= 9:
        return "summer"
    elif 9 <= month or month <= 1:
        return "S1"


def get_current_git_tag_name():
    try:
        repo = Repo('.', search_parent_directories=True)
        for tag in repo.tags:
            if tag.commit.hexsha == repo.head.commit.hexsha:
                return tag.name
        return repo.active_branch.name
    except GitError:
        return ""
