import datetime
from urllib.parse import urlencode

from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import JsonResponse, Http404
from django.shortcuts import render, redirect
# Create your views here.
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic.list import ListView
from django.views.decorators.debug import sensitive_post_parameters

from gaia_compta.models import OperationUtilisateur
from gaia_distribs.models import InscriptionAdherent
from gaia_users.forms import AdhesionForm, InscriptionForm, ConfirmEmailForm
from gaia_users.models import Adhesion, Utilisateur, EmailConfirmationCode
from gaia_utils.models import GaiaHypertext, GaiaSetting


class ProfileHomePageView(View):
    template_name = "users/profile/home.html"

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        first_incription_adherent = InscriptionAdherent.objects.filter(
            contrat__souscripteur=request.user,
            distribution__date_heure__gte=datetime.datetime.now(),
            contrat__date_signature__isnull=False
        ).order_by("distribution__date_heure").first()
        next_distribution = None
        produits_next_distribution = {}

        if first_incription_adherent is not None:
            next_distribution = first_incription_adherent.distribution
            inscriptions = InscriptionAdherent.objects.filter(
                distribution=next_distribution, contrat__souscripteur=request.user,
                contrat__date_signature__isnull=False
            )

            for inscription in inscriptions:
                if inscription.produit.id not in produits_next_distribution:
                    produits_next_distribution[inscription.produit.id] = {"name": inscription.produit.nom, "amount": 0}
                produits_next_distribution[inscription.produit.id]["amount"] += inscription.unites_commandees

        popup_frama = GaiaHypertext.find_content("ACCUEIL_UTILISATEUR_INFOS")

        return render(request, self.template_name, {
            "user": request.user,
            "account_confirmed": request.user.statut != Utilisateur.Statuts.NON_CONFIRME,
            "next_distribution": next_distribution,
            "produits_next_distribution": produits_next_distribution,
            "ACCUEIL_UTILISATEUR_INFOS": popup_frama
        })

class ProfileEditView(View):
    template_name = "users/profile/my_profile.html"

    @method_decorator(login_required)
    def get(self, request):
        return render(request, self.template_name, {})

class RegisterAdhesionView(View):
    template_name = "users/adhesion/register_adhesion.html"


    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.is_legitimate, login_url="email_not_confirmed_page", redirect_field_name=None
    ))
    def get(self, request):
        choices = self.get_year_choices(request.user.id)
        form = AdhesionForm(choices)
        tarif_adhesion = GaiaSetting.get_value("TARIF_ADHESION")

        try:
            if "annee" in request.GET:
                preselected_annee = int(request.GET.get("annee"))
                if preselected_annee in form.choices:
                    form.initial['date_adhesion'] = preselected_annee

        except (ValueError, TypeError):
            pass

        fragment = GaiaHypertext.find_content("PAGE_ADHERER")

        return render(request, self.template_name, {
            "tarif": tarif_adhesion,
            "form": form,
            "PAGE_ADHERER": fragment
        })

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.is_legitimate, login_url="email_not_confirmed_page", redirect_field_name=None
    ))
    def post(self, request):
        choices = self.get_year_choices(request.user.id)
        form = AdhesionForm(choices, request.POST)
        tarif_adhesion = GaiaSetting.get_value("TARIF_ADHESION")

        fragment = GaiaHypertext.find_content("PAGE_ADHERER")

        if form.is_valid():
            adhesion = Adhesion(annee=form.cleaned_data["date_adhesion"],
                                tarif=tarif_adhesion,
                                payee=False,
                                date_enregistrement=datetime.date.today(),
                                utilisateur=request.user)
            adhesion.save()
            transaction = OperationUtilisateur(
                montant=adhesion.tarif,
                ferme=None,
                contrat=None,
                utilisateur=adhesion.utilisateur,
                date_enregistrement=datetime.datetime.now(),
                libelle="Adhésion du {0} pour l'année {1}".format(
                    adhesion.date_enregistrement.strftime("%d/%m/%Y"), adhesion.annee
                ),
                statut=OperationUtilisateur.Statuts.OPERATION_ENREGISTREE,
            )
            transaction.save()

            next_url = request.GET.get('next', '')
            if next_url:
                return redirect(next_url)  # TODO check to prevent external redirection
            else:
                choices = self.get_year_choices(request.user.id)
                form = AdhesionForm(choices)

                return render(request, self.template_name, {
                    "tarif": tarif_adhesion,
                    "form": form,
                    "adhesion_success": adhesion.annee,
                    "PAGE_ADHERER": fragment
                })

        return render(request, self.template_name, {
            "tarif": tarif_adhesion,
            "form": form,
            "PAGE_ADHERER": fragment
        })

    def get_year_choices(self, user_id):
        adhesions = Adhesion.objects.filter(utilisateur_id__exact=user_id).filter(
            annee__gte=datetime.date.today().year - 1,
            annee__lt=datetime.date.today().year + 2
        )

        choices = []
        today = datetime.date.today()
        if today.month >= 2:
            choices = [today.year + i for i in range(2)]  # this year and the next
        else:
            choices = [today.year + i - 1 for i in
                       range(2)]  # last year (= current_semester) and current year (= next semester)

        already_member_years = [adhesion.annee for adhesion in adhesions]
        choices = [c for c in choices if c not in already_member_years]

        return choices


# TODO annuler une adhésion si pas encore utilisée ni payée ?


class MyAdhesionsView(ListView):
    template_name = "users/adhesion/my_adhesions.html"
    model = Adhesion
    context_object_name = 'adhesions'
    paginate_by = 10
    
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        adhesions = Adhesion.objects.filter(utilisateur_id=user.id).order_by("-annee")
        return adhesions

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        this_year = datetime.date.today().year
        subscribed = any(adhesion.annee == this_year for adhesion in context['adhesions'])
        fragment = GaiaHypertext.find_content("PAGE_MES_ADHESIONS")
        context['current_year'] = {
            "year": this_year,
            "subscribed": subscribed
        }
        context["PAGE_MES_ADHESIONS"] = fragment
        return context

class RegisterView(View):
    template_name = "users/registration/register.html"

    @method_decorator(sensitive_post_parameters())
    def get(self, request):
        form = InscriptionForm()

        return render(request, self.template_name, {
            "form": form
        })

    @method_decorator(sensitive_post_parameters())
    def post(self, request):
        form = InscriptionForm(request.POST)

        if form.is_valid():
            data = form.cleaned_data
            if Utilisateur.objects.filter(email__exact=data["email"]).count() == 0:
                user = Utilisateur.objects.create_user(
                    username=data["email"],
                    email=data["email"],
                    password=data["password"],
                    first_name=data["first_name"],
                    last_name=data["last_name"],
                    home_address=data["post_address"] if data["post_address"] else None,
                    phone_number=data["phone"] if data["phone"] else None,
                    statut=Utilisateur.Statuts.NON_CONFIRME
                )
                user.send_confirmation_email()

                url = reverse("login")
                args = urlencode({
                    "message": "register_success"
                })

                return redirect("{}?{}".format(url, args))
            else:
                form.add_error(
                    None, "Cette adresse email est déjà utilisée. Veuillez en choisir une autre."
                )

        return render(request, self.template_name, {
            "form": form
        })


def has_user_not_confirmed_email(user: Utilisateur):
    return not user.is_legitimate(user)


class SendConfirmationEmailView(View):
    template_name = "users/userspace.html"

    def get(self, request, user_id):
        user = Utilisateur.get_or_404(user_id)
        if Utilisateur.is_legitimate(user):
            return Http404()

        json = "json" in request.GET and request.GET.get("json")

        success = user.send_confirmation_email()

        base_html = '<div class="container fs-5 mt-5">{0}</div>'

        if not json:
            if success:
                return render(request, self.template_name, {
                    "generic_template_content": base_html.format("Email envoyé avec succès !")
                })
            else:
                return render(request, self.template_name, {
                    "generic_template_content": base_html.format("Erreur lors de l'envoi de l'email. "
                                                                 "Contactez-nous afin de résoudre le problème.")
                })
        else:
            return JsonResponse({
                "success": success,
                "message": "Email envoyé avec succès" if success else "Erreur lors de l'envoi de l'email. "
                                                                      "Contactez-nous afin de résoudre le problème."
            })


class ConfirmEmailView(View):
    template_name = "users/registration/confirm_email.html"

    def get(self, request, user_id):
        user = Utilisateur.get_or_404(user_id)
        if Utilisateur.is_legitimate(user):
            return redirect(to="home_page")

        code = request.GET.get('code', None)
        form = ConfirmEmailForm(request.GET)

        if code:
            success = self.handle_submission(user, code)
            if success:
                return redirect(to="profile_home")
            else:
                form.add_error(
                    None,
                    "Le code fourni automatiquement était invalide, veuillez renseigner un code valide manuellement."
                )

        return render(request, self.template_name, {
            "form": form,
            "user": user
        })

    def post(self, request, user_id):
        user = Utilisateur.get_or_404(user_id)
        if Utilisateur.is_legitimate(user):
            return redirect(to="home_page")

        form = ConfirmEmailForm(request.POST)
        if form.is_valid():
            success = self.handle_submission(user, form.cleaned_data["code"])
            if success:
                return redirect(to="profile_home")
            else:
                form.add_error(
                    None,
                    "Le code fourni est invalide."
                )

        return render(request, self.template_name, {
            "form": form,
            "user": user
        })

    def handle_submission(self, user, code):
        try:
            stored_code = EmailConfirmationCode.objects.get(
                user=user,
                code=code
            )

            if stored_code.time_emitted + datetime.timedelta(minutes=15) > datetime.datetime.now():
                stored_code.delete()
                user.statut = Utilisateur.Statuts.UTILISATEUR
                user.save()

                return True

            return False

        except EmailConfirmationCode.DoesNotExist:
            return False


class GetAllUsersView(View):
    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_gestionnaire, login_url="profile_home", redirect_field_name=None))
    def get(self, request):
        utilisateurs = Utilisateur.objects.filter(is_active=True).order_by("last_name", "first_name").all()
        data = [{"id": utilisateur.id, "text": utilisateur.last_name + " " + utilisateur.first_name + "(" + utilisateur.email + ")"} for utilisateur in utilisateurs]
        return JsonResponse(data, safe=False)