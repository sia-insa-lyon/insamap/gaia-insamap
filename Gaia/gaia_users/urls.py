from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView, PasswordResetView, \
    PasswordResetDoneView, PasswordResetConfirmView, PasswordResetCompleteView
from django.urls import path, reverse_lazy
from django.views.generic import TemplateView

from gaia_users.forms import LoginForm, GaiaPasswordResetForm, GaiaSetPasswordForm
from gaia_users.views import ProfileHomePageView, ProfileEditView, RegisterAdhesionView, MyAdhesionsView, RegisterView, ConfirmEmailView, \
    SendConfirmationEmailView, GetAllUsersView

urlpatterns = [
    path('register', RegisterView.as_view(), name="register"),
    path('login', LoginView.as_view(template_name="users/registration/login.html",
                                    form_class=LoginForm), name="login"),
    path('logout', LogoutView.as_view(template_name="users/registration/logged_out.html"), name="logout"),

    path('password-change',
         PasswordChangeView.as_view(template_name="users/registration/password_change_form.html", success_url=reverse_lazy("profile_edit")),
         name="password_change"),

    path('password-reset',
         PasswordResetView.as_view(
             success_url=reverse_lazy("password_reset_done"),
             template_name="users/registration/password_reset/password_reset_form.html",
             form_class=GaiaPasswordResetForm,
             email_template_name="users/registration/password_reset/password_reset_email_raw.html",
             subject_template_name="users/registration/password_reset/password_reset_email_subject.txt",
             html_email_template_name="users/registration/password_reset/password_reset_email.html"),
         name="password_reset"),
    path('password-reset/email-sent',
         PasswordResetDoneView.as_view(
             template_name="users/registration/password_reset/password_reset_email_sent.html"),
         name="password_reset_done"),

    path('password-reset/choice/<uidb64>/<token>',
         PasswordResetConfirmView.as_view(
             template_name="users/registration/password_reset/password_reset_choice.html",
             form_class=GaiaSetPasswordForm
         ),
         name="password_reset_choice"),
    path('password-reset/complete',
         PasswordResetCompleteView.as_view(
             template_name="users/registration/password_reset/password_reset_complete.html"
         ),
         name="password_reset_complete"),

    path('email_not_confirmed',
         login_required(
             TemplateView.as_view(template_name="users/registration/email_not_confirmed.html"),
             redirect_field_name=None
         ),
         name="email_not_confirmed_page"),
    path('<int:user_id>/confirm_email', ConfirmEmailView.as_view(), name="confirm_email"),
    path('<int:user_id>/send_confirmation_email', SendConfirmationEmailView.as_view(), name="send_confirmation_email"),

    path('', ProfileHomePageView.as_view(), name="profile_home"),
    path('edit', ProfileEditView.as_view(), name="profile_edit"),

    path('adhesions', MyAdhesionsView.as_view(), name="my_adhesions"),
    path('adhesions/adherer', RegisterAdhesionView.as_view(), name="register_adhesion"),

    path('get_all_users', GetAllUsersView.as_view(), name='get_all_users'),
]
