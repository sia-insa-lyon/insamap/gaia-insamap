import inspect
import logging
import random
import string
from datetime import datetime, timedelta

from django.contrib.auth.models import AbstractUser
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.core.mail import send_mail
from django.db import models
from django.http import Http404
from django.template.loader import get_template
from django.contrib import admin

from Gaia import settings


class Utilisateur(AbstractUser):
    class Statuts(models.IntegerChoices):
        UTILISATEUR = 1
        PAYSAN = 2
        GESTIONNAIRE = 3
        NON_CONFIRME = 4

    email = models.EmailField(verbose_name='Adresse email', blank=False, unique=True)
    first_name = models.CharField("Prénom", max_length=150, blank=False)
    last_name = models.CharField("Nom", max_length=150, blank=False)

    username_validator = UnicodeUsernameValidator()
    username = models.CharField(
        "Pseudo",
        max_length=150,
        blank=True,
        null=False,
        unique=True,
        help_text="Non utilisé, a vocation à disparaitre. 150 caractères ou moins. Lettres, chiffres et @/./+/-/_ uniquement.",
        validators=[username_validator],
        error_messages={
            'unique': "Un utilisateur avec ce pseudo existe déjà",
        },
    )

    phone_number = models.CharField(verbose_name="Numéro de téléphone", max_length=25, blank=True, null=True)
    home_address = models.CharField(verbose_name="Adresse postale", blank=True, max_length=256, null=True)
    statut = models.IntegerField(choices=Statuts.choices)

    essai_gratuit = models.BooleanField(verbose_name="Dispose de la période d'essai initiale ?", default=True, null=False)

    REQUIRED_FIELDS = ['first_name', 'last_name', 'statut', 'username']
    USERNAME_FIELD = 'email'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __str__(self):
        return "{0} {1} ({2})".format(self.last_name, self.first_name, self.email)

    def is_legitimate(self):
        return self.statut != Utilisateur.Statuts.NON_CONFIRME

    def is_paysan(self):
        has_farms = self.fermes.count() > 0 or self.fermes_representees.count() > 0
        return self.statut == Utilisateur.Statuts.PAYSAN or has_farms

    def is_gestionnaire(self):
        return self.statut == Utilisateur.Statuts.GESTIONNAIRE

    def can_access_farmspace(self):
        return Utilisateur.is_paysan(self) or Utilisateur.is_gestionnaire(self)

    @admin.display(description="Adhérent", boolean=True)
    def adherent(self):
        return Utilisateur.is_adherent(self)

    @staticmethod
    def is_adherent(user):
        adhesions = Adhesion.objects.filter(utilisateur_id__exact=user.id).filter(annee__exact=datetime.now().year).all()
        return len(adhesions) > 0

    @staticmethod
    def get_or_404(user_id):
        try:
            utilisateur = Utilisateur.objects.get(id=user_id)
            return utilisateur
        except Utilisateur.DoesNotExist:
            raise Http404()

    def send_confirmation_email(self):
        try:
            previous_code = EmailConfirmationCode.objects.get(
                user_id__exact=self.id
            )
            previous_code.delete()
        except EmailConfirmationCode.DoesNotExist:  # do nothing if no code existed before
            pass

        chars = string.ascii_uppercase + string.digits
        code_string = ''.join(random.choice(chars) for i in range(10))

        code_object = EmailConfirmationCode(
            user=self,
            code=code_string,
            time_emitted=datetime.now()
        )
        code_object.save()

        html_template = get_template(
            "users/registration/email_confirmation_mail.html"
        )
        raw_template = get_template(
            "users/registration/email_confirmation_mail_raw.html"
        )

        mail_subject = "[Gaia] Confirmation de votre adresse email"
        site_name = "https://" + settings.SITE_NAME

        html_version = html_template.render({
            "first_name": self.first_name,
            "code": code_string,
            "expiration_time": code_object.time_emitted + timedelta(minutes=15),
            "subject": mail_subject,
            "site_name": site_name,
            "user_id": self.id
        })

        raw_version = raw_template.render({
            "first_name": self.first_name,
            "code": code_string,
            "expiration_time": code_object.time_emitted + timedelta(minutes=15),
            "subject": mail_subject,
            "site_name": site_name,
            "user_id": self.id
        })

        try:
            mail_sent = send_mail(
                subject=mail_subject,
                message=raw_version,
                from_email=settings.EMAIL_HOST_USER,
                recipient_list=[self.email],
                html_message=html_version
            )  # TODO stop ignoring if mail wasn't send
            return True
        except Exception as e:
            logging.getLogger("User").critical(
                "Une erreur est survenue lors de l'envoi d'un mail ({0}:{1}) : ".format(
                    __file__, inspect.currentframe().f_lineno
                ) + str(e)
            )
            return False


class EmailConfirmationCode(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, primary_key=True, null=False)
    code = models.CharField(max_length=128, null=False, blank=False)
    time_emitted = models.DateTimeField(null=False)


class Adhesion(models.Model):
    annee = models.PositiveIntegerField(null=False, blank=False)
    tarif = models.FloatField(null=False, blank=False)
    payee = models.BooleanField(default=False)
    date_enregistrement = models.DateField(null=False, blank=False)

    utilisateur = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=False)

    def __str__(self):
        return "Adhésion de {4}, faite le {0} pour l'année {1} ({2}€ {3})".format(
            self.date_enregistrement.strftime("%d/%m/%Y"),
            self.annee, self.tarif, "payés" if self.payee else "non payés",
            self.utilisateur.email
        )
