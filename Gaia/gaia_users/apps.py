from django.apps import AppConfig


class GaiaUsersConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gaia_users'
    verbose_name = "Gaia - Utilisateurs"
