import datetime

from django import forms
from django.contrib.auth.forms import AuthenticationForm, PasswordResetForm, SetPasswordForm
from django.contrib.auth.password_validation import validate_password, get_password_validators
from django.core.exceptions import ValidationError

from Gaia import settings


class AdhesionForm(forms.Form):
    date_adhesion = forms.TypedChoiceField(label="Vous souhaitez faire une demande d'adhésion pour l'année",
                                           required=True, widget=forms.Select(attrs={"class": "form-control"}),
                                           coerce=int, empty_value=None)

    def __init__(self, year_choices, *args, **kwargs):
        super(AdhesionForm, self).__init__(*args, **kwargs)

        select_choices = [(year, year) for year in year_choices]
        self.choices = year_choices
        self.fields['date_adhesion'].choices = select_choices
        self.fields['date_adhesion'].initial = datetime.date.today().year


class InscriptionForm(forms.Form):
    email = forms.EmailField(required=True, label="Adresse email",
                             widget=forms.EmailInput(attrs={"class": "form-control"}))
    password = forms.CharField(required=True, label="Mot de passe", min_length=8,
                               widget=forms.PasswordInput(attrs={"class": "form-control"}))
    password_confirm = forms.CharField(required=True, label="Confirmez le mot de passe",
                                       widget=forms.PasswordInput(attrs={"class": "form-control"}), min_length=8)

    first_name = forms.CharField(required=True, label="Prénom", min_length=1,
                                 widget=forms.TextInput(attrs={"class": "form-control"}))
    last_name = forms.CharField(required=True, label="Nom", min_length=1,
                                widget=forms.TextInput(attrs={"class": "form-control"}))

    phone = forms.CharField(required=False, label="Numéro de téléphone",
                            widget=forms.TextInput(attrs={"class": "form-control"}),
                            help_text="Optionnel, utilisé pour vous contacter si besoin.")
    post_address = forms.CharField(required=False, label="Adresse postale",
                                   widget=forms.TextInput(attrs={"class": "form-control"}),
                                   help_text="Optionnel, utilisé pour vous contacter si besoin.")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.label_suffix = ''

    def clean(self):
        cleaned_data = super().clean()

        email = self.cleaned_data["email"]
        password = self.cleaned_data["password"]
        first_name = self.cleaned_data["first_name"]
        last_name = self.cleaned_data["last_name"]

        validate_password(password, password_validators=get_password_validators(
            settings.AUTH_PASSWORD_VALIDATORS))  # raises ValidationError

        if email and password and first_name and last_name:
            if email.lower() in password.lower() or \
                    first_name.lower() in password.lower() or \
                    last_name.lower() in password.lower():
                raise ValidationError("Votre mot de passe ne doit pas contenir votre email, nom ou prénom !")

        data = self.cleaned_data["password_confirm"]
        if self.cleaned_data["password"] != data:
            raise ValidationError("Les deux mots de passe doivent être identiques.")

        return cleaned_data


class LoginForm(AuthenticationForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['username'].widget = forms.TextInput(
            attrs={
                'autofocus': True,
                'class': 'form-control'
            }
        )

        self.fields['password'].widget = forms.PasswordInput(
            attrs={
                'autocomplete': 'current-password',
                'class': 'form-control'
            }
        )


class ConfirmEmailForm(forms.Form):
    code = forms.CharField(max_length=64, required=True, strip=True, widget=forms.TextInput(
        attrs={
            "class": "form-control",
            "style": "max-width: 10em"
        }
    ))


class GaiaPasswordResetForm(PasswordResetForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['email'].widget = forms.EmailInput(
            attrs={
                'autofocus': True,
                'class': 'form-control'
            }
        )


class GaiaSetPasswordForm(SetPasswordForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['new_password1'].widget = forms.PasswordInput(
            attrs={
                'autofocus': True,
                'class': 'form-control',
                'autocomplete': 'new-password'
            }
        )

        self.fields['new_password2'].widget = forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'autocomplete': 'new-password'
            }
        )

