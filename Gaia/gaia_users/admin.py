from django.contrib import admin
from django.urls import reverse
from gaia_users.models import Utilisateur, Adhesion
from datetime import datetime
from django.contrib.auth.models import Group

class IsActiveFilter(admin.SimpleListFilter):
    title = "état de l'utilisateur"
    parameter_name = "actif"
    
    def lookups(self, request, model_admin):
        return [
            (None, "Utilisateur actif"),
            ("non_active", "Utilisateur désactivé"),
            ("all", "Tous")
        ]
    
    def choices(self, cl):
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == lookup,
                'query_string': cl.get_query_string({
                    self.parameter_name: lookup,
                }, []),
                'display': title,
            }

    def filter_for_Utilisateur(self, queryset):
        if self.value() == "non_active":
                return queryset.filter(is_active=False)
        if self.value() == None:
            return queryset.filter(is_active=True)
        else:
            return queryset
    
    def filter_for_Adhesion(self, queryset):
        if self.value() == "non_active":
                return queryset.filter(utilisateur__is_active=False)
        if self.value() == None:
            return queryset.filter(utilisateur__is_active=True)
        else:
            return queryset
    
    def queryset(self, request, queryset):
        if queryset.model == Utilisateur:
            return self.filter_for_Utilisateur(queryset)
        if queryset.model == Adhesion:
            return self.filter_for_Adhesion(queryset)
        else:
            return queryset

class AnneeAdhesionFilter(admin.SimpleListFilter):
    title = "année"
    parameter_name = "annee"

    def lookups(self, request, model_admin):
        return [
            (str(datetime.now().year-1), str(datetime.now().year-1)),
            (str(datetime.now().year), str(datetime.now().year)),
            (str(datetime.now().year+1), str(datetime.now().year+1))
        ]

    def queryset(self, request, queryset):
        if self.value() == str(datetime.now().year-1):
            return Adhesion.objects.filter(annee__exact=datetime.now().year-1)
        if self.value() == str(datetime.now().year):
            return Adhesion.objects.filter(annee__exact=datetime.now().year)
        if self.value() == str(datetime.now().year+1):
            return Adhesion.objects.filter(annee__exact=datetime.now().year+1)
        else:
            return queryset

admin.site.unregister(Group)

@admin.register(Utilisateur)
class UtilisateurAdminPanel(admin.ModelAdmin):
    fieldsets = (
        ('Informations personnelles', {
            'fields': ('last_name', 'first_name', 'email', 'phone_number', 'home_address')
        }),
        ('Statut et authentification', {
            'fields': ('statut', 'essai_gratuit', 'is_active', 'username', 'password')
        }),
        ('Permissions', {
            'fields': ('is_staff', 'is_superuser', 'groups', 'user_permissions'),
            'classes': ('collapse',)
        }),
    )
    filter_horizontal = ('groups', 'user_permissions',)
    list_display = ('last_name', 'first_name', 'email', 'adherent', 'essai_gratuit')
    list_display_links = ('last_name', 'first_name', 'email')
    list_filter = ('statut',IsActiveFilter,)
    radio_fields = {"statut": admin.HORIZONTAL}
    search_fields = ('last_name', 'first_name', 'email',)

    def view_on_site(self, obj):
        return reverse("user_details_admin", args=[obj.id])

@admin.register(Adhesion)
class AdhesionAdminPanel(admin.ModelAdmin):
    actions = ["mark_as_paid", "delete_selected"]
    fields = ('utilisateur', 'annee', 'payee', 'date_enregistrement', 'tarif')
    list_display = ('utilisateur', 'annee', 'payee',)
    list_display_links = ('utilisateur', 'annee')
    list_filter = (AnneeAdhesionFilter, 'payee',IsActiveFilter,)
    autocomplete_fields = ("utilisateur",)

    @admin.action(description="Marquer comme payée")
    def mark_as_paid(self, request, queryset):
        queryset.update(payee=True)
