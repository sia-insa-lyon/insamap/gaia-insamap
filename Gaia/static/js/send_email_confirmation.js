function registerConfirmationEmailEvent(button_id) {
    const email_conf_btn = $("#" + button_id);

    email_conf_btn.click(() => {
        email_conf_btn.attr("disabled", true);

        $.ajax({
            url: "/account/send_confirmation_email?json=true",
            success: (response) => {
                alert(response.message);
            },
            error: (xhr, status, error) => {
                alert("Une erreur est survenue lors de l'envoi de l'email. " +
                    "Veuillez vérifier votre connexion à Internet. " +
                    "Si le problème persiste, contactez-nous afin de le résoudre. " +
                    "Erreur : " + error + " " + status);
            },
            complete: () => {
                setTimeout(() => {
                    email_conf_btn.attr("disabled", false);
                }, 10000);
            }
        });
    });
}

