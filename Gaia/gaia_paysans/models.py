from django.core.exceptions import PermissionDenied
from django.db import models
from django.forms import ValidationError
from django.http import Http404

from Gaia import settings
from gaia_users.models import Utilisateur
from gaia_paysans.fields import CheckBoxArrayField

class Ferme(models.Model):
    nom = models.CharField(blank=False, null=False, max_length=100)
    paysans = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name="fermes")

    infos_paiement = models.TextField(default="Pas d'informations spécifiées.", help_text="Modes de paiement acceptés, etc.")
    infos_commande = models.TextField(default="", help_text="Date limite commande, etc.", blank=True, null=False, verbose_name="Informations commandes")
    type_activite = models.CharField(default="Activité non renseignée", max_length=100,
                                     verbose_name='Type de produits vendus, par exemple "Légumes et fruits de saison"')
    description = models.TextField(default="", max_length=500, verbose_name="Courte description publique",
                                   help_text="500 caractères maximum.", blank=True, null=False)
    affichee_publiquement = models.BooleanField(default=False, verbose_name="Afficher la ferme sur la page d'accueil ?")

    entite_juridique = models.CharField(null=False, blank=False, max_length=100)
    adresse = models.CharField(null=False, blank=False, max_length=250)

    representant = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, null=False,
                                     related_name="fermes_representees")
    
    archive = models.BooleanField(default=False)

    def clean_fields(self, exclude=None):
        super().clean_fields(exclude=exclude)
        if self.archive == True:
            produits = Produit.objects.filter(ferme=self, archive=False)
            if produits.exists():
                raise ValidationError("Impossible d'archiver une ferme contenant des produits non archivés.")
    
    def infos_paiement_html(self):
        return self.infos_paiement.replace("\n", "<br>").replace("\r", "")

    def __str__(self):
        return self.nom

    def user_has_access(self, user):
        return Utilisateur.is_gestionnaire(user) or user == self.representant or user in self.paysans

    def check_access(self, user):
        if not self.user_has_access(user):
            raise PermissionDenied

    @staticmethod
    def get_or_404(ferme_id):
        try:
            ferme = Ferme.objects.get(id=ferme_id)
            return ferme
        except Ferme.DoesNotExist:
            raise Http404()


class Produit(models.Model):

    class Frequences(models.TextChoices):
        HEBDOMADAIRE_4 = "hebdomadaire_4", "Livraison hebdomadaire pour 4 distributions",
        HEBDOMADAIRE_SEMESTRE = "hebdomadaire_semestre", "Livraison hebdomadaire pour tout le semestre",
        BIMENSUELLE_4 = "bimensuelle_4", "Livraison une distribution sur deux pour 4 distributions",
        BIMENSUELLE_SEMESTRE = "bimensuelle_semestre", "Livraison une distribution sur deux pour tout le semestre",
        UNIQUE = "unique", "Livraison unique"

        @staticmethod
        def get_default_frequences():
            return list([Produit.Frequences.HEBDOMADAIRE_4, Produit.Frequences.HEBDOMADAIRE_SEMESTRE])
    
    nom = models.CharField(blank=False, max_length=50)
    short_name = models.CharField(null=True, blank=True, max_length=10, verbose_name="Nom court")
    description = models.CharField(default="", max_length=1000, blank=True)
    
    ferme = models.ForeignKey(Ferme, on_delete=models.CASCADE, null=False)

    tarif_reference = models.FloatField(verbose_name="Tarif de référence", null=False)
    quantite_reference = models.CharField(blank=False, max_length=20)

    frequences = CheckBoxArrayField(models.CharField(choices=Frequences.choices,max_length=50), verbose_name="Fréquences", default=Frequences.get_default_frequences, help_text="Pour la livraison unique, vous devez cocher également la case 'Autoriser en livraison unique ?' pour les distributions concernées.")

    archive = models.BooleanField(default=False)

    def clean_fields(self, exclude=None):
        super().clean_fields(exclude=exclude)
        if self.archive == True:
            distributions_id = self.inscriptions_produits.values_list('distribution', flat=True).distinct()
            distributions = Distribution.objects.filter(id__in=distributions_id, archive=False)
            if distributions.exists():
                raise ValidationError("Impossible d'archiver un produit inscrit à des distributions non archivées")
    
    def inscriptions_between(self, start_date, end_date):
        inscriptions = self.inscriptions_produits.filter(
            distribution__date_heure__range=(start_date, end_date)
        )
        return inscriptions

    def __str__(self):
        return "Ferme '{0}' - {1} (QU: {2}, PU: {3:.2f} €)".format(
            self.ferme.nom, self.nom, self.quantite_reference, self.tarif_reference
        )
    
    def get_frequences_display(self):
        labels = dict(Produit.Frequences.choices)
        return [labels[f] for f in self.frequences]
    
    @staticmethod
    def get_or_404(produit_id):
        try:
            produit = Produit.objects.get(id=produit_id)
            return produit
        except Produit.DoesNotExist:
            raise Http404()

from gaia_distribs.models import Distribution