from django.apps import AppConfig

class GaiaPaysansConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gaia_paysans'
    verbose_name = "Gaia - Paysans"
    
    def ready(self):
        import gaia_paysans.signals
