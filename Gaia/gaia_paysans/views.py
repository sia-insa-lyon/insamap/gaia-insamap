import datetime
import json
from io import BytesIO
from json import JSONDecodeError

from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.exceptions import PermissionDenied
from django.db import transaction, IntegrityError
from django.db.models import Q, Sum
from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest
from django.shortcuts import render
from django.template.loader import get_template
from django.utils.decorators import method_decorator
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views import View
from gaia_utils.models import GaiaHypertext
from xhtml2pdf import pisa

from gaia_compta.models import OperationUtilisateur
from gaia_distribs import utils
from gaia_distribs.models import InscriptionProduit, Distribution, InscriptionAdherent, Contrat
from gaia_paysans.forms import RegisterProductForDeliveryForm, FermeSignerContratForm
from gaia_paysans.models import Ferme, Produit
from gaia_users.models import Utilisateur


class MyFermesView(View):
    template_name = "paysans/mes_fermes.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_paysan, login_url="profile_home", redirect_field_name=None))
    # @method_decorator(permission_required("gaia_paysans.view_ferme", raise_exception=True))
    def get(self, request):
        fermes = Ferme.objects.filter(
            Q(paysans=request.user) | Q(representant=request.user)
        ).all()

        fragment = GaiaHypertext.find_content("PAGE_MES_FERMES")

        return render(request, self.template_name, {
            "fermes": fermes,
            "PAGE_MES_FERMES": fragment
        })

class FermeHub(View):
    template_name = "paysans/ferme_hub.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.can_access_farmspace, login_url="profile_home", redirect_field_name=None))
    # @method_decorator(permission_required("gaia_paysans.view_ferme", raise_exception=True))
    def get(self, request, ferme_id):
        ferme = Ferme.get_or_404(ferme_id)
        ferme.check_access(request.user)

        signatures_pending = InscriptionProduit.objects.filter(
            produit__ferme_id=ferme.id, signature_amap=True, signature_ferme=False, distribution__date_heure__gte=datetime.datetime.now()
        ).count()

        next_distrib = Distribution.objects.filter(
            inscriptions_produits__isnull=False,
            date_heure__date__gte=datetime.datetime.today(),
            inscriptions_adherents__contrat__ferme_id=ferme.id
        ).order_by("date_heure").first()
        inscriptions_next_distrib = None

        if next_distrib is not None:
            inscriptions_next_distrib = {}
            inscriptions = InscriptionAdherent.objects.filter(
                distribution=next_distrib, contrat__ferme=ferme, contrat__date_signature__isnull=False
            )
            for inscription in inscriptions:
                if inscription.produit.nom not in inscriptions_next_distrib:
                    inscriptions_next_distrib[inscription.produit.nom] = 0
                inscriptions_next_distrib[inscription.produit.nom] += inscription.unites_commandees

        return render(request, self.template_name, {
            "ferme": ferme,
            "next_delivery": next_distrib,
            "next_delivery_content": inscriptions_next_distrib,
            "signatures_pending": signatures_pending
        })


class ManageProductsView(View):
    template_name = "paysans/manage_products.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.can_access_farmspace, login_url="profile_home", redirect_field_name=None))
    # @method_decorator(permission_required("gaia_paysans.view_produit", raise_exception=True))
    def get(self, request, ferme_id):
        ferme = Ferme.get_or_404(ferme_id)
        ferme.check_access(request.user)

        produits = Produit.objects.filter(ferme=ferme)

        return render(request, self.template_name, {
            "ferme": ferme,
            "produits": produits
        })


class RegisterProductForDeliveriesView(View):
    template_name = "paysans/register_products_distribution.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.can_access_farmspace, login_url="profile_home", redirect_field_name=None))
    # @method_decorator(permission_required("gaia_paysans.view_produit", raise_exception=True))
    # @method_decorator(permission_required("gaia_distribs.view_distribution", raise_exception=True))
    # @method_decorator(permission_required("gaia_distribs.change_inscriptionproduit", raise_exception=True))
    def get(self, request, ferme_id):
        ferme = Ferme.get_or_404(ferme_id)
        ferme.check_access(request.user)

        form = RegisterProductForDeliveryForm(ferme_id)

        return render(request, self.template_name, {
            "form": form,
            "ferme": ferme
        })

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.can_access_farmspace, login_url="profile_home", redirect_field_name=None))
    # @method_decorator(permission_required("gaia_paysans.view_produit", raise_exception=True))
    # @method_decorator(permission_required("gaia_distribs.view_distribution", raise_exception=True))
    # @method_decorator(permission_required("gaia_distribs.change_inscriptionproduit", raise_exception=True))
    def post(self, request, ferme_id):
        ferme = Ferme.get_or_404(ferme_id)
        ferme.check_access(request.user)

        form = RegisterProductForDeliveryForm(ferme_id, request.POST)
        success_display = None
        if form.is_valid():
            soumission_amap = request.user.is_gestionnaire()

            distributions = form.cleaned_data["distributions"]
            produit = form.cleaned_data["produit"]
            unites_dispo = form.cleaned_data["unites_dispo"]
            jours_cloture = form.cleaned_data["jours_avant_cloture"]

            tarif_vente = form.cleaned_data["tarif_vente"]
            quantite_unitaire = form.cleaned_data["quantite_unitaire"]

            registered_dates = []
            last_distrib_date = None
            quantite_unitaire_finale = None
            tarif_vente_final = None
            try:
                with transaction.atomic():
                    for distribution in distributions:
                        last_date = distribution.date_heure
                        inscription = InscriptionProduit(
                            produit=produit,
                            distribution=distribution,
                            unites_dispo=unites_dispo,
                            delai_max_fermeture_inscriptions=jours_cloture,
                            tarif_vente=tarif_vente,
                            quantite_unitaire=quantite_unitaire if len(quantite_unitaire) > 0 else None,
                            unites_restantes=unites_dispo,
                            signature_amap=soumission_amap,
                            signature_ferme=(not soumission_amap)
                        )
                        inscription.save()
                        registered_dates.append(distribution.date_heure.strftime("%d/%m/%Y"))
                        quantite_unitaire_finale = inscription.quantite_unitaire
                        tarif_vente_final = inscription.tarif_vente

            except IntegrityError:
                form.add_error(
                    None,
                    "Impossible de créer la distribution pour la date %s : violation de contrainte d'intégrité. "
                    "Veuillez contacter un administateur du site." % last_date.strftime("%d/%m/%Y")
                )
                return render(request, self.template_name, {
                    "form": form,
                    "ferme": ferme
                })

            success_display = {
                "dates": ', '.join(registered_dates),
                "units": unites_dispo,
                "days": jours_cloture,
                "product": produit.nom,
                "price_per_unit": tarif_vente_final,
                "amount_per_unit": quantite_unitaire_finale
            }

            form = RegisterProductForDeliveryForm(ferme_id)
            return render(request, self.template_name, {
                "form": form,
                "ferme": ferme,
                "success_display": success_display
            })

        return render(request, self.template_name, {
            "form": form,
            "ferme": ferme
        })


class FermeSignerContratsView(View):
    template_name = "paysans/signer_contrats.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_paysan, login_url="profile_home", redirect_field_name=None))
    def get(self, request, ferme_id):
        ferme = Ferme.get_or_404(ferme_id)
        ferme.check_access(request.user)

        inscriptions = InscriptionProduit.objects.filter(
            signature_amap=True, signature_ferme=False, produit__ferme=ferme, distribution__date_heure__gte=datetime.datetime.now()
        ).order_by("distribution__date_heure")

        return render(request, self.template_name, {
            "inscriptions": inscriptions,
            "ferme": ferme
        })

    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_paysan, login_url="profile_home", redirect_field_name=None))
    def post(self, request, ferme_id):
        ferme = Ferme.get_or_404(ferme_id)
        ferme.check_access(request.user)

        inscriptions = InscriptionProduit.objects.filter(
            signature_amap=True, signature_ferme=False, produit__ferme=ferme, distribution__date_heure__gte=datetime.datetime.now()
        )

        form = FermeSignerContratForm(inscriptions, request.POST)
        if form.is_valid():
            for fieldname, value in form.cleaned_data.items():
                inscription_id = int(fieldname.split("_")[1])
                inscription = inscriptions.get(id=inscription_id)
                inscription.signature_ferme = value
                inscription.save()

        inscriptions = InscriptionProduit.objects.filter(
            signature_amap=True, signature_ferme=False, produit__ferme=ferme, distribution__date_heure__gte=datetime.datetime.now()
        )

        return render(request, self.template_name, {
            "inscriptions": inscriptions,
            "ferme": ferme
        })


class DeliverySheetAbstractMixin():

    def get_delivery_info(self, distribution_id, ferme_id):
        distribution = Distribution.get_or_404(distribution_id)

        inscriptions_adherents = InscriptionAdherent.objects.filter(
            distribution_id=distribution.id, contrat__ferme_id=ferme_id, contrat__date_signature__isnull=False
        )

        # list of dicts {name, short_name}
        headers = []
        headers_columns = dict()  # produit_id => headers array index

        product_quantities = dict()
        totals = []

        user_info = []
        user_columns = dict()

        for inscription in inscriptions_adherents.distinct("produit"):
            produit = inscription.produit

            headers.append({"name": produit.nom})
            headers[len(headers) - 1]["short_name"] = produit.short_name if produit.short_name else None

            totals.append(0)

            product_quantities[len(headers)] = produit.quantite_reference  # TODO does not handle variable quantites
            headers_columns[produit.id] = len(headers) - 1

        total_to_pay = 0.0
        for inscription in inscriptions_adherents.order_by(
                "contrat__souscripteur__last_name", "contrat__souscripteur__first_name"):
            user = inscription.contrat.souscripteur

            if user.id not in user_columns:
                user_columns[user.id] = len(user_info)
                user_info.append({
                    "name": user.last_name.upper() + " " + user.first_name,
                    "amounts": [0 for i in range(len(headers))],
                    "to_pay": 0.0
                })

            header_column = headers_columns[inscription.produit.id]
            user_column = user_columns[user.id]
            user_info[user_column]["amounts"][header_column] += inscription.unites_commandees
            totals[header_column] += inscription.unites_commandees

            montant = round(inscription.unites_commandees * inscription.inscription_produit.tarif_vente, 2)
            user_info[user_column]["to_pay"] += montant
            total_to_pay += montant

        return {
            "headers": headers,
            "distribution": distribution,
            "user_info": user_info,
            "product_quantities": product_quantities,
            "totals": totals,
            "total_to_pay": total_to_pay
        }


class FeuilleLivraisonPDFView(View, DeliverySheetAbstractMixin):
    template_name = "paysans/delivery_sheet.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.can_access_farmspace, login_url="profile_home", redirect_field_name=None))
    def get(self, request, ferme_id, id_distribution):
        ferme = Ferme.get_or_404(ferme_id)
        ferme.check_access(request.user)

        delivery_sheet_info = self.get_delivery_info(id_distribution, ferme.id)
        should_include_prices = True if request.GET.get("with_prices", False) else False
        delivery_sheet_info["include_prices"] = should_include_prices

        template = get_template(self.template_name)
        html = template.render(delivery_sheet_info)

        result = BytesIO()
        pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), result)
        return HttpResponse(result.getvalue(), content_type="application/pdf")


class FeuilleLivraisonCSVView(View, DeliverySheetAbstractMixin):
    template_name = "paysans/delivery_sheet_csv.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.can_access_farmspace, login_url="profile_home", redirect_field_name=None))
    def get(self, request, ferme_id, id_distribution):
        ferme = Ferme.get_or_404(ferme_id)
        ferme.check_access(request.user)

        delivery_sheet_info = self.get_delivery_info(id_distribution, ferme.id)
        should_include_prices = True if request.GET.get("with_prices", False) else False
        delivery_sheet_info["include_prices"] = should_include_prices

        template = get_template(self.template_name)
        csv = template.render(delivery_sheet_info)

        result = BytesIO(csv.encode("Windows-1252"))
        return HttpResponse(result.getvalue(), content_type="text/csv")


class FarmDeliveriesListView(View):
    template_name = "paysans/deliveries/deliveries_list.html"
    paginate_by = 25

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.can_access_farmspace, login_url="profile_home", redirect_field_name=None))
    def get(self, request, ferme_id):
        ferme = Ferme.get_or_404(ferme_id)
        ferme.check_access(request.user)

        deliveries = Distribution.objects.filter(
            inscriptions_produits__produit__ferme=ferme,
            date_heure__gte=datetime.datetime.now()
        ).order_by("date_heure").distinct()

        deliveries_list = []

        for delivery in deliveries:
            deliveries_list.append({
                "id": delivery.id,
                "datetime_start": delivery.date_heure,
                "datetime_end": delivery.date_heure_fin(),
                "location": delivery.lieu,
                "commands": {}
            })

            commands = InscriptionAdherent.objects.filter(
                distribution_id=delivery.id,
                contrat__ferme_id=ferme.id,
                contrat__date_signature__isnull=False
            )

            for command in commands:
                commands_dict = deliveries_list[-1]["commands"]
                if command.produit.nom not in commands_dict:
                    commands_dict[command.produit.nom] = 0
                commands_dict[command.produit.nom] += command.unites_commandees

        paginator = Paginator(deliveries_list, self.paginate_by)
        page = request.GET.get('page')
        try:
            deliveries_page = paginator.page(page)
        except PageNotAnInteger:
            deliveries_page = paginator.page(1)
        except EmptyPage:
            deliveries_page = paginator.page(paginator.num_pages)

        return render(request, self.template_name, {
            "ferme": ferme,
            "deliveries": deliveries_page
        })


class FarmPaymentsListView(View):
    template_name = "paysans/accounting/operations_list.html"
    paginate_by = 25

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.can_access_farmspace, login_url="profile_home", redirect_field_name=None))
    def get(self, request, ferme_id):
        ferme = Ferme.get_or_404(ferme_id)
        ferme.check_access(request.user)

        date_interval, period, year = self.extract_request_parameters(request)

        operations = OperationUtilisateur.objects.filter(
            ferme_id=ferme.id,
            date_enregistrement__gte=date_interval[0],
            date_enregistrement__lt=date_interval[1],
        ).order_by("utilisateur__last_name", "utilisateur__first_name", "-date_enregistrement")

        paginator = Paginator(operations, self.paginate_by)
        page = request.GET.get('page')
        try:
            operations_page = paginator.page(page)
        except PageNotAnInteger:
            operations_page = paginator.page(1)
        except EmptyPage:
            operations_page = paginator.page(paginator.num_pages)

        operation_years = OperationUtilisateur.objects.filter(ferme_id=ferme.id).distinct("date_enregistrement__year")
        year_choices = [operation.date_enregistrement.year for operation in operation_years]

        return render(request, self.template_name, {
            "ferme": ferme,
            "operations": operations_page,
            "year": year,
            "period": period,
            "year_choices": year_choices
        })

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.can_access_farmspace, login_url="profile_home", redirect_field_name=None))
    def post(self, request, ferme_id):
        ferme = Ferme.get_or_404(ferme_id)
        ferme.check_access(request.user)

        try:
            json_data = json.loads(request.body)
            self.try_update_operations(ferme, json_data)

        except (JSONDecodeError, ValueError):
            return HttpResponseBadRequest("Invalid JSON content.")
        except OperationUtilisateur.DoesNotExist:
            return HttpResponseBadRequest("Une des opérations n'existe pas ou plus.")
        except PermissionError:
            return HttpResponseBadRequest(
                "Vous ne pouvez pas modifier des opérations qui ne concernent pas votre ferme !")

        return JsonResponse({
            "success": True
        })

    def extract_request_parameters(self, request):
        period = request.GET.get("period")
        if period and period not in ["S1", "S2", "summer"]:
            period = None

        if period is None:
            period = utils.get_semester_name_from_date(datetime.date.today())

        year = request.GET.get("year")
        try:
            year = int(year)
        except (TypeError, ValueError) as e:
            year = datetime.datetime.today().year

        return utils.get_date_interval_from_semester_name(period, year), period, year

    def try_update_operations(self, ferme, json_data):
        for id_operation, new_value in json_data[0].items(): # Statut payé ou non
            operation = OperationUtilisateur.objects.get(id=id_operation)
            if operation.ferme is None or operation.ferme_id != ferme.id:
                raise PermissionError

            if new_value == True:
                operation.previous_statut = operation.statut
                operation.statut = operation.Statuts.FERME_RECU
            elif new_value == False:
                operation.statut = operation.previous_statut if operation.previous_statut is not None else operation.Statuts.OPERATION_ENREGISTREE
                operation.previous_statut = operation.Statuts.FERME_RECU
            operation.save()

        for id_operation, new_value in json_data[1].items(): # Mode de paiement
            operation = OperationUtilisateur.objects.get(id=id_operation)
            if operation.ferme is None or operation.ferme_id != ferme.id:
                raise PermissionError
            operation.mode_paiement = new_value if new_value else None
            operation.save()

class FarmPaymentsCsvView(View):
    template_name = "paysans/accounting/operations_list_csv.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.can_access_farmspace, login_url="profile_home", redirect_field_name=None))
    def get(self, request, ferme_id):
        ferme = Ferme.get_or_404(ferme_id)
        ferme.check_access(request.user)

        date_interval, period, year = self.extract_request_parameters(request)

        operations = OperationUtilisateur.objects.filter(
            ferme_id=ferme.id,
            date_enregistrement__gte=date_interval[0],
            date_enregistrement__lt=date_interval[1],
        ).order_by("utilisateur__last_name", "utilisateur__first_name", "-date_enregistrement")

        payments_infos = {
            "operations": operations,
            "year": year,
            "period": period,
            "ferme": ferme
        }

        template = get_template(self.template_name)
        csv = template.render(payments_infos)

        result = BytesIO(csv.encode("Windows-1252"))
        return HttpResponse(result.getvalue(), content_type="text/csv")

    def extract_request_parameters(self, request):
        period = request.GET.get("period")
        if period and period not in ["S1", "S2", "summer"]:
            period = None

        if period is None:
            period = utils.get_semester_name_from_date(datetime.date.today())

        year = request.GET.get("year")
        try:
            year = int(year)
        except (TypeError, ValueError) as e:
            year = datetime.datetime.today().year

        return utils.get_date_interval_from_semester_name(period, year), period, year


class FarmContractsListView(View):
    template_name = "paysans/contract/contracts_list.html"
    paginate_by = 25

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.can_access_farmspace, login_url="profile_home", redirect_field_name=None))
    def get(self, request, ferme_id):
        ferme = Ferme.get_or_404(ferme_id)
        ferme.check_access(request.user)

        period = request.GET.get("period")
        if period and period not in ["S1", "S2", "summer"]:
            period = None

        if period is None:
            month = datetime.datetime.today().month
            if 2 <= month <= 5:
                period = "S2"
            elif 6 <= month <= 9:
                period = "summer"
            elif 9 <= month or month <= 1:
                period = "S1"

        year = request.GET.get("year")
        try:
            year = int(year)
        except (TypeError, ValueError) as e:
            year = datetime.datetime.today().year

        date_interval = None  # is [inclusive; exclusive[
        if period == "S2":
            date_interval = [
                datetime.date(year=year, month=2, day=1),
                datetime.date(year=year, month=6, day=1)
            ]
        elif period == "summer":
            date_interval = [
                datetime.date(year=year, month=6, day=1),
                datetime.date(year=year, month=10, day=1)
            ]
        elif period == "S1":
            date_interval = [
                datetime.date(year=year, month=10, day=1),
                datetime.date(year=year + 1, month=2, day=1)
            ]

        contracts = Contrat.objects.filter(
            ferme_id=ferme.id,
            date_signature__isnull=False,
            date_signature__gte=date_interval[0],
            date_signature__lt=date_interval[1]
        ).order_by("-date_signature")

        contract_years = Contrat.objects.filter(
            ferme_id=ferme.id, date_signature__isnull=False
        ).distinct("date_signature__year")
        year_choices = [contract.date_signature.year for contract in contract_years]

        paginator = Paginator(contracts, self.paginate_by)
        page = request.GET.get('page')
        try:
            contracts_page = paginator.page(page)
        except PageNotAnInteger:
            contracts_page = paginator.page(1)
        except EmptyPage:
            contracts_page = paginator.page(paginator.num_pages)

        return render(request, self.template_name, {
            "ferme": ferme,
            "contracts": contracts_page,
            "year": year,
            "period": period,
            "year_choices": year_choices
        })


class FarmContractDetailsView(View):
    template_name = "paysans/contract/contract_details.html"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(
        Utilisateur.can_access_farmspace, login_url="profile_home", redirect_field_name=None))
    def get(self, request, ferme_id, contract_id):
        ferme = Ferme.get_or_404(ferme_id)
        ferme.check_access(request.user)

        contract = Contrat.objects.get(id=contract_id)
        if contract.ferme_id != ferme.id:
            raise PermissionDenied

        products_by_delivery = {}
        deliveries = {}
        products = {}
        commands = InscriptionAdherent.objects.filter(
            contrat_id=contract.id
        )

        for command in commands:
            delivery_id = command.distribution.id
            if delivery_id not in products_by_delivery:
                deliveries[delivery_id] = command.distribution
                products_by_delivery[delivery_id] = {}

            product_id = command.produit.id
            if product_id not in products_by_delivery[delivery_id]:
                products[product_id] = command.inscription_produit
                products_by_delivery[delivery_id][product_id] = {"amount": 0, "total_price": 0}

            info_dict = products_by_delivery[delivery_id][product_id]
            info_dict["amount"] += command.unites_commandees
            info_dict["total_price"] = info_dict["amount"] * command.inscription_produit.tarif_vente

        return render(request, self.template_name, {
            "ferme": ferme,
            "contract": contract,
            "product_amounts": products_by_delivery,
            "deliveries": deliveries,
            "products": products
        })

class GetAllFarmsView(View):
    @method_decorator(login_required)
    @method_decorator(user_passes_test(Utilisateur.is_gestionnaire, login_url="profile_home", redirect_field_name=None))
    def get(self, request):
        fermes = Ferme.objects.filter(archive=False).order_by("nom").all()
        data = [{"id": ferme.id, "text": ferme.nom} for ferme in fermes]
        return JsonResponse(data, safe=False)