from gaia_paysans.models import Produit
from django.db.models.signals import post_save
from django.dispatch import receiver
from datetime import datetime


@receiver(post_save, sender=Produit)
def update_archive_produit(sender, instance, created, **kwargs):
    # Si le produit est dé-archivé, on dé-archive la ferme
    if instance.archive == False and instance.ferme and instance.ferme.archive == True:
        instance.ferme.archive = False
        instance.ferme.save()
