from datetime import datetime

from django import forms
from django.core.exceptions import ValidationError
from django.db.models import Q

from gaia_distribs.models import Distribution
from gaia_paysans.models import Produit


class RegisterProductForDeliveryForm(forms.Form):
    produit = forms.ModelChoiceField(
        queryset=None,
        empty_label=None,
        label="Produit à inscrire",
        required=True,
        widget=forms.Select(attrs={"class": "form-control"})
    )

    unites_dispo = forms.IntegerField(
        min_value=1, initial=1, label="Nombre d'unités du produit disponibles",
        required=True,
        widget=forms.NumberInput(attrs={"class": "form-control"})
    )

    jours_avant_cloture = forms.IntegerField(
        min_value=1, initial=4,
        label="Délai d'inscription",
        help_text="Nombre de jours avant la distribution pour la clôture des inscriptions. "
                  "Les inscriptions fermeront la veille du jour ciblé, à 23:59.",
        required=True,
        widget=forms.NumberInput(attrs={"class": "form-control"})
    )

    distributions = forms.ModelMultipleChoiceField(
        queryset=None,
        label="Dates des livraisons",
        help_text="Maintenez Ctrl appuyé et cliquez sur les créneaux pour en choisir plusieurs "
                  "(Ctrl+Shift pour sélectionner une plage).",
        required=True,
        widget=forms.SelectMultiple(attrs={"class": "form-control"})
    )

    tarif_vente = forms.FloatField(
        required=False,
        label="Tarif unitaire spécifique",
        help_text="Tarif unitaire effectif du produit. Laisser vide si identique au prix de référence du produit."
    )

    quantite_unitaire = forms.CharField(
        required=False,
        label="Quantité unitaire spécifique",
        help_text="Quantité unitaire effective du produit. Laisser vide si identique à la quantité de référence du produit."
    )

    def __init__(self, ferme_id, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["produit"].queryset = Produit.objects.filter(ferme_id=ferme_id)
        self.fields["distributions"].queryset = Distribution.objects.filter(
            date_heure__gte=datetime.now()
        ).filter(
            Q(date_visibilite_paysans__isnull=True)
            | Q(date_visibilite_paysans__gte=datetime.now())
        ).order_by("date_heure")

    def clean(self):
        cleaned_data = super().clean()
        product = cleaned_data["produit"]
        distributions = cleaned_data["distributions"]

        available_distributions = Distribution.objects.exclude(inscriptions_produits__produit=product).exclude(
            date_heure__lt=datetime.now())
        rejected_distributions = []

        for distribution in distributions:
            if distribution not in available_distributions:
                rejected_distributions.append(distribution.date_heure.strftime("%d/%m/%Y"))

        if len(rejected_distributions) > 0:
            dates = ', '.join(rejected_distributions)

            raise ValidationError({
                "distributions":
                    "Le produit sélectionné a déjà été attribué aux distributions suivantes : " + dates + ". "
                    "Veuillez retirer les enregistrements précédents, ou bien ne sélectionnez pas "
                    "ces distributions pour cet enregistrement."
            })


class FermeSignerContratForm(forms.Form):

    def __init__(self, inscriptions, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for inscription in inscriptions:
            self.fields[
                "inscription_" + str(inscription.id)
            ] = forms.BooleanField(initial=False, required=False)

