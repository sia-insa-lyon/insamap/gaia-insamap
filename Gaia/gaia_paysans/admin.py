from typing import Any
from django.contrib import admin
from django.http import HttpRequest
from django.urls import reverse

from gaia_paysans.models import Ferme, Produit
from gaia_distribs.admin import InscriptionProduitInline
from gaia_distribs.models import InscriptionProduit, InscriptionAdherent
from gaia_users.models import Utilisateur
from gaia_distribs.admin import ArchiveFilter

from datetime import datetime

@admin.register(Ferme)
class FermeAdminPanel(admin.ModelAdmin):
    fieldsets = (
        ("Informations Ferme", {
            "fields": ("nom", "representant", "type_activite", "description", "entite_juridique", "adresse", "infos_paiement", "infos_commande",)
        }),
        ("Visibilité", {
            "fields": ("affichee_publiquement", "paysans",)
        }),
        ("Statut", {
            "fields": ("archive",),
            "classes": ("collapse",)
        })
    )
    filter_horizontal = ("paysans",)
    list_display = ("nom", "representant", "display_type_activite",)
    list_display_links = ("nom",)
    list_filter= (ArchiveFilter,)
    search_fields = ("nom",)
    autocomplete_fields = ("representant",)
    show_full_result_count = False

    def display_type_activite(self, obj):
        return obj.type_activite
    display_type_activite.short_description = "Type d'activté"

    def view_on_site(self, obj):
        return reverse("farm_details_admin", args=[obj.id])
    
    def get_search_results(self, request, queryset, search_term):
        queryset, use_distinct = super().get_search_results(request, queryset, search_term)
        # Si la recherche n'est pas effectuée depuis la page d'administration des fermes
        if not request.path.endswith("/gaia_paysans/ferme/"):
            queryset = queryset.filter(archive=False)
        queryset = queryset.order_by("nom")
        return queryset, use_distinct
    
    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == "paysans":
            kwargs["queryset"] = Utilisateur.objects.filter(is_active=True, statut=Utilisateur.Statuts.PAYSAN)
        return super(FermeAdminPanel, self).formfield_for_manytomany(db_field, request, **kwargs)


@admin.register(Produit)
class ProduitAdminPanel(admin.ModelAdmin):
    fieldsets = (
        ("Informations Produit", {
            "fields": ("nom", "short_name", "description", "ferme",)
        }),
        ("Informations Distributions", {
            "fields": ("frequences", "tarif_reference", "quantite_reference",)
        }),
        ("Statut", {
            "fields": ("archive",),
            "classes": ("collapse",)
        })
    )


    list_display = ("nom", "ferme", "quantite_reference" , "tarif_reference",)
    list_display_links = ("nom", "ferme",)
    list_filter = (ArchiveFilter, ("ferme", admin.RelatedFieldListFilter),)
    autocomplete_fields = ("ferme",)
    search_fields = ("nom",)
    inlines = (InscriptionProduitInline,)
    show_full_result_count = False

    def get_search_results(self, request, queryset, search_term):
        queryset, use_distinct = super().get_search_results(request, queryset, search_term)
        queryset = queryset.order_by("ferme__nom", "nom")
        # Si la recherche n'est pas effectuée depuis la page d'administration des produits
        if not request.path.endswith("/gaia_paysans/produit/"):
            queryset = queryset.filter(archive=False)
        return queryset, use_distinct
    
    def view_on_site(self, obj):
        return reverse("product_details_admin", args=[obj.id])
    
    def get_readonly_fields(self, request, obj=None):
        if obj is not None and InscriptionAdherent.objects.filter(produit=obj).exists():
            return ("ferme",)
        return ()
    


