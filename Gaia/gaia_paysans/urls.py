from django.urls import path

from gaia_paysans.views import MyFermesView, FermeHub, ManageProductsView, RegisterProductForDeliveriesView, \
    FermeSignerContratsView, FeuilleLivraisonPDFView, FeuilleLivraisonCSVView, FarmDeliveriesListView, \
    FarmPaymentsListView, FarmContractsListView, FarmContractDetailsView, FarmPaymentsCsvView, GetAllFarmsView

urlpatterns = [
    path('mes-fermes', MyFermesView.as_view(), name="mes_fermes"),

    path('ferme/<int:ferme_id>', FermeHub.as_view(), name="ferme_hub"),

    path('ferme/<int:ferme_id>/produits', ManageProductsView.as_view(), name="ferme_manage_products"),
    path('ferme/<int:ferme_id>/register_delivery', RegisterProductForDeliveriesView.as_view(),
         name="ferme_register_product_distributions"),

    path('ferme/<int:ferme_id>/contrats', FarmContractsListView.as_view(), name="farm_contracts_list"),
    path('ferme/<int:ferme_id>/signer_contrats', FermeSignerContratsView.as_view(), name="ferme_signer_contrats"),
    path('ferme/<int:ferme_id>/contrat/<int:contract_id>', FarmContractDetailsView.as_view(),
         name="farm_contract_details"),

    path('ferme/<int:ferme_id>/livraisons', FarmDeliveriesListView.as_view(), name="farm_deliveries_list"),
    path('ferme/<int:ferme_id>/livraison/<int:id_distribution>/feuille/pdf', FeuilleLivraisonPDFView.as_view(),
         name="feuille_livraison_pdf"),
    path('ferme/<int:ferme_id>/livraison/<int:id_distribution>/feuille/csv', FeuilleLivraisonCSVView.as_view(),
         name="feuille_livraison_csv"),

    path('ferme/<int:ferme_id>/accounting/operations', FarmPaymentsListView.as_view(), name="farm_payments_list"),
    path('ferme/<int:ferme_id>/accounting/operations/csv', FarmPaymentsCsvView.as_view(), name="farm_payments_csv"),

    path('get_all_farms', GetAllFarmsView.as_view(), name="all_farms")

]
