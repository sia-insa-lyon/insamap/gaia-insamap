# Generated by Django 3.2.7 on 2024-04-14 22:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gaia_paysans', '0006_remove_produit_modifiable'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ferme',
            name='infos_commande',
            field=models.TextField(blank=True, default='', help_text='Date limite commande, etc.', verbose_name='Informations commandes'),
        ),
    ]
